//---------------------------------------------------------------------------

#include <sdlpp/app.h>
#include <sdlpp/window.h>

#include <sdlpp/mouse.h>
#include <sdlpp/keyboard.h>

#include <spdlog/spdlog.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        app::app(app_options options, const allocator_type & alloc) :
            on_event(alloc),
            _windows(alloc)
        {
            if (SDL_Init(options.flags) < 0) {
                BOOST_THROW_EXCEPTION(exception("Couldn't initialize SDL!"));
            }

#if BOOST_OS_MACOS
            SDL_SetHint(SDL_HINT_MOUSE_TOUCH_EVENTS, "1");
#endif
        }

        app::~app() {
            SDL_Quit();
        }

        bool app::process_events() {
            if (_need_close) {
                return false;
            }

            while (SDL_PollEvent(&_event) != 0) {
                if (_event.type == SDL_QUIT) {
                    return false;
                }

                this->on_event(_event);
            }

            return true;
        }

        void app::quit() {
            _need_close = true;
        }
    }
}

//---------------------------------------------------------------------------
