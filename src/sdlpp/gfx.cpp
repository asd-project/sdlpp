//---------------------------------------------------------------------------

#include <sdlpp/gfx.h>
#include <spdlog/spdlog.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        static int get_profile_mask(opengl::profile profile) {
            switch (profile) {
                case opengl::profile::core:
                    return SDL_GL_CONTEXT_PROFILE_CORE;
                case opengl::profile::compatibility:
                    return SDL_GL_CONTEXT_PROFILE_COMPATIBILITY;
                case opengl::profile::es:
                    return SDL_GL_CONTEXT_PROFILE_ES;
            }

            BOOST_ASSERT_MSG(false, "[sdlpp][get_profile_mask] Invalid opengl::profile value");
            return SDL_GL_CONTEXT_PROFILE_CORE;
        }

        graphics::graphics(sdl::window & window, const opengl::configuration & config) : opengl::graphics(config), _window(window) {
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, get_profile_mask(config.profile));
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, config.major);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, config.minor);
#if GL_DEBUG
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, config.flags | SDL_GL_CONTEXT_DEBUG_FLAG);
#else
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, config.flags);
#endif

            SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

// #if !(ASD_PLAT_EMSCRIPTEN || ASD_PLAT_WSL)
//             SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
//             SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8);
// #endif

            _context = SDL_GL_CreateContext(window);

            if (_context == nullptr) {
                BOOST_THROW_EXCEPTION(exception("Can't create graphics"));
            }

            SDL_GL_MakeCurrent(window, _context);

            glewExperimental = GL_TRUE;
            auto error_code = glewInit();

            if (error_code != GLEW_OK) {
                BOOST_THROW_EXCEPTION(glew_exception(error_code, "Error initializing GLEW!"));
            }

#if !ASD_PLAT_WSL
            if (SDL_GL_SetSwapInterval(1) < 0) {    // enable v-sync
                BOOST_THROW_EXCEPTION(exception("Warning: Unable to enable v-sync!"));
            }
#endif

            // TODO: move to opengl::graphics
            OPENGL_TRY(glDepthFunc(GL_LESS));

            OPENGL_TRY(glFrontFace(GL_CCW));

            set_cull_face(GL_BACK);
            set_blending(gfx::blending::interpolative);

#if GL_DEBUG
#if !ASD_OPENGL_ES
            if (glDebugMessageCallback != nullptr) {
                OPENGL_TRY(glEnable(GL_DEBUG_OUTPUT));
                OPENGL_TRY(glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS));

                spdlog::debug("glDebugMessageCallback is supported");
                OPENGL_TRY(glDebugMessageCallback(opengl::glDebugCallbackFunc, nullptr));
            } else {
                spdlog::debug("glDebugMessageCallback is not supported");
            }
#else
            spdlog::debug("glDebugMessageCallback is not supported");
#endif
#endif
        }
    }
}

//---------------------------------------------------------------------------
