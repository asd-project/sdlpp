//---------------------------------------------------------------------------

#include <sdlpp/window.h>
#include <meta/flag.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        window::window(sdl::app & app, const sdl::window_title & title, const window_size & size) :
            _app(app),
            _logical_size(size),
            _event_observer(app.on_event, {[this](const SDL_Event & e) {
                switch (e.type) {
                    case SDL_WINDOWEVENT: {
                        if (this->id() == e.window.windowID) {
                            process_event(e.window);
                        }

                        break;
                    }
                }
            }})
        {
#if ASD_PLAT_EMSCRIPTEN
            _handle = SDL_CreateWindow(title.value.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, size.x - 1, size.y, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
            SDL_SetWindowSize(_handle, size.x, size.y);
#elif BOOST_OS_MACOS
            _handle = SDL_CreateWindow(title.value.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, size.x, size.y, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);
#else
            _handle = SDL_CreateWindow(title.value.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, static_cast<int>(size.x), static_cast<int>(size.y), SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
#endif
            if (_handle == nullptr) {
                BOOST_THROW_EXCEPTION(exception("Couldn't create window!"));
            }

            SDL_SetWindowMinimumSize(_handle, 100, 50);
            SDL_GL_GetDrawableSize(_handle, &_physical_size.x, &_physical_size.y);

            SDL_RaiseWindow(_handle);

            _app.register_window(this);
        }

        window::~window() {
            _app.unregister_window(this);

            if (_handle != nullptr) {
                SDL_DestroyWindow(_handle);
            }
        }

        uint32 window::id() const {
            return SDL_GetWindowID(_handle);
        }

        bool window::active() const {
            return check_flag(SDL_WINDOW_INPUT_FOCUS, SDL_GetWindowFlags(_handle));
        }

        void window::resize(const window_size & size) {
            SDL_SetWindowSize(_handle, static_cast<int>(size.x), static_cast<int>(size.y));
        }

        void window::process_event(const SDL_WindowEvent & e) {
            switch (e.event) {
                case SDL_WINDOWEVENT_RESIZED: [[ fallthrough ]];
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    SDL_GL_GetDrawableSize(_handle, &_physical_size.x, &_physical_size.y);
                    on_resize(_logical_size = window_size{e.data1, e.data2}, _physical_size);
                    return;
            }
        }
    }
}

//---------------------------------------------------------------------------
