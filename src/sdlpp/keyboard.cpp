//---------------------------------------------------------------------------

#include <SDL_keycode.h>
#include <sdlpp/keyboard.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        static uio::keycode cast_keycode(SDL_Keycode key) {
            switch(key) {
                case SDLK_RETURN:
                    return uio::keycode::enter;
                case SDLK_ESCAPE:
                    return uio::keycode::escape;
                case SDLK_BACKSPACE:
                    return uio::keycode::backspace;
                case SDLK_TAB:
                    return uio::keycode::tab;
                case SDLK_SPACE:
                    return uio::keycode::space;
                case SDLK_EXCLAIM:
                    return uio::keycode::exclaimation;
                case SDLK_QUOTEDBL:
                    return uio::keycode::doublequote;
                case SDLK_HASH:
                    return uio::keycode::hash;
                case SDLK_PERCENT:
                    return uio::keycode::percent;
                case SDLK_DOLLAR:
                    return uio::keycode::dollar;
                case SDLK_AMPERSAND:
                    return uio::keycode::ampersand;
                case SDLK_QUOTE:
                    return uio::keycode::quote;
                case SDLK_LEFTPAREN:
                    return uio::keycode::parentesis_left;
                case SDLK_RIGHTPAREN:
                    return uio::keycode::parentesis_right;
                case SDLK_ASTERISK:
                    return uio::keycode::asterisk;
                case SDLK_PLUS:
                    return uio::keycode::plus;
                case SDLK_COMMA:
                    return uio::keycode::comma;
                case SDLK_MINUS:
                    return uio::keycode::minus;
                case SDLK_PERIOD:
                    return uio::keycode::period;
                case SDLK_SLASH:
                    return uio::keycode::slash;
                case SDLK_0:
                    return uio::keycode::num_0;
                case SDLK_1:
                    return uio::keycode::num_1;
                case SDLK_2:
                    return uio::keycode::num_2;
                case SDLK_3:
                    return uio::keycode::num_3;
                case SDLK_4:
                    return uio::keycode::num_4;
                case SDLK_5:
                    return uio::keycode::num_5;
                case SDLK_6:
                    return uio::keycode::num_6;
                case SDLK_7:
                    return uio::keycode::num_7;
                case SDLK_8:
                    return uio::keycode::num_8;
                case SDLK_9:
                    return uio::keycode::num_9;
                case SDLK_COLON:
                    return uio::keycode::colon;
                case SDLK_SEMICOLON:
                    return uio::keycode::semicolon;
                case SDLK_LESS:
                    return uio::keycode::less;
                case SDLK_EQUALS:
                    return uio::keycode::equal;
                case SDLK_GREATER:
                    return uio::keycode::greater;
                case SDLK_QUESTION:
                    return uio::keycode::question;
                case SDLK_AT:
                    return uio::keycode::at;
                case SDLK_LEFTBRACKET:
                    return uio::keycode::bracket_left;
                case SDLK_BACKSLASH:
                    return uio::keycode::backslash;
                case SDLK_RIGHTBRACKET:
                    return uio::keycode::bracket_right;
                case SDLK_CARET:
                    return uio::keycode::caret;
                case SDLK_UNDERSCORE:
                    return uio::keycode::underscore;
                case SDLK_BACKQUOTE:
                    return uio::keycode::backquote;
                case SDLK_a:
                    return uio::keycode::a;
                case SDLK_b:
                    return uio::keycode::b;
                case SDLK_c:
                    return uio::keycode::c;
                case SDLK_d:
                    return uio::keycode::d;
                case SDLK_e:
                    return uio::keycode::e;
                case SDLK_f:
                    return uio::keycode::f;
                case SDLK_g:
                    return uio::keycode::g;
                case SDLK_h:
                    return uio::keycode::h;
                case SDLK_i:
                    return uio::keycode::i;
                case SDLK_j:
                    return uio::keycode::j;
                case SDLK_k:
                    return uio::keycode::k;
                case SDLK_l:
                    return uio::keycode::l;
                case SDLK_m:
                    return uio::keycode::m;
                case SDLK_n:
                    return uio::keycode::n;
                case SDLK_o:
                    return uio::keycode::o;
                case SDLK_p:
                    return uio::keycode::p;
                case SDLK_q:
                    return uio::keycode::q;
                case SDLK_r:
                    return uio::keycode::r;
                case SDLK_s:
                    return uio::keycode::s;
                case SDLK_t:
                    return uio::keycode::t;
                case SDLK_u:
                    return uio::keycode::u;
                case SDLK_v:
                    return uio::keycode::v;
                case SDLK_w:
                    return uio::keycode::w;
                case SDLK_x:
                    return uio::keycode::x;
                case SDLK_y:
                    return uio::keycode::y;
                case SDLK_z:
                    return uio::keycode::z;

                case SDLK_UNKNOWN:
                default:
                    return uio::keycode::unknown;
            }
        }

        static uio::scancode cast_scancode(SDL_Scancode code) {
            switch(code) {
                case SDL_SCANCODE_A:
                    return uio::scancode::a;
                case SDL_SCANCODE_B:
                    return uio::scancode::b;
                case SDL_SCANCODE_C:
                    return uio::scancode::c;
                case SDL_SCANCODE_D:
                    return uio::scancode::d;
                case SDL_SCANCODE_E:
                    return uio::scancode::e;
                case SDL_SCANCODE_F:
                    return uio::scancode::f;
                case SDL_SCANCODE_G:
                    return uio::scancode::g;
                case SDL_SCANCODE_H:
                    return uio::scancode::h;
                case SDL_SCANCODE_I:
                    return uio::scancode::i;
                case SDL_SCANCODE_J:
                    return uio::scancode::j;
                case SDL_SCANCODE_K:
                    return uio::scancode::k;
                case SDL_SCANCODE_L:
                    return uio::scancode::l;
                case SDL_SCANCODE_M:
                    return uio::scancode::m;
                case SDL_SCANCODE_N:
                    return uio::scancode::n;
                case SDL_SCANCODE_O:
                    return uio::scancode::o;
                case SDL_SCANCODE_P:
                    return uio::scancode::p;
                case SDL_SCANCODE_Q:
                    return uio::scancode::q;
                case SDL_SCANCODE_R:
                    return uio::scancode::r;
                case SDL_SCANCODE_S:
                    return uio::scancode::s;
                case SDL_SCANCODE_T:
                    return uio::scancode::t;
                case SDL_SCANCODE_U:
                    return uio::scancode::u;
                case SDL_SCANCODE_V:
                    return uio::scancode::v;
                case SDL_SCANCODE_W:
                    return uio::scancode::w;
                case SDL_SCANCODE_X:
                    return uio::scancode::x;
                case SDL_SCANCODE_Y:
                    return uio::scancode::y;
                case SDL_SCANCODE_Z:
                    return uio::scancode::z;

                case SDL_SCANCODE_1:
                    return uio::scancode::num_1;
                case SDL_SCANCODE_2:
                    return uio::scancode::num_2;
                case SDL_SCANCODE_3:
                    return uio::scancode::num_3;
                case SDL_SCANCODE_4:
                    return uio::scancode::num_4;
                case SDL_SCANCODE_5:
                    return uio::scancode::num_5;
                case SDL_SCANCODE_6:
                    return uio::scancode::num_6;
                case SDL_SCANCODE_7:
                    return uio::scancode::num_7;
                case SDL_SCANCODE_8:
                    return uio::scancode::num_8;
                case SDL_SCANCODE_9:
                    return uio::scancode::num_9;
                case SDL_SCANCODE_0:
                    return uio::scancode::num_0;

                case SDL_SCANCODE_RETURN:
                    return uio::scancode::enter;
                case SDL_SCANCODE_ESCAPE:
                    return uio::scancode::escape;
                case SDL_SCANCODE_BACKSPACE:
                    return uio::scancode::backspace;
                case SDL_SCANCODE_TAB:
                    return uio::scancode::tab;
                case SDL_SCANCODE_SPACE:
                    return uio::scancode::space;

                case SDL_SCANCODE_MINUS:
                    return uio::scancode::minus;
                case SDL_SCANCODE_EQUALS:
                    return uio::scancode::equals;
                case SDL_SCANCODE_LEFTBRACKET:
                    return uio::scancode::bracket_left;
                case SDL_SCANCODE_RIGHTBRACKET:
                    return uio::scancode::bracket_right;
                case SDL_SCANCODE_BACKSLASH:
                    return uio::scancode::backslash;

                case SDL_SCANCODE_SEMICOLON:
                    return uio::scancode::semicolon;
                case SDL_SCANCODE_APOSTROPHE:
                    return uio::scancode::apostrophe;
                case SDL_SCANCODE_GRAVE:
                    return uio::scancode::grave;
                case SDL_SCANCODE_COMMA:
                    return uio::scancode::comma;
                case SDL_SCANCODE_PERIOD:
                    return uio::scancode::period;
                case SDL_SCANCODE_SLASH:
                    return uio::scancode::slash;

                case SDL_SCANCODE_CAPSLOCK:
                    return uio::scancode::caps_lock;

                case SDL_SCANCODE_F1:
                    return uio::scancode::f1;
                case SDL_SCANCODE_F2:
                    return uio::scancode::f2;
                case SDL_SCANCODE_F3:
                    return uio::scancode::f3;
                case SDL_SCANCODE_F4:
                    return uio::scancode::f4;
                case SDL_SCANCODE_F5:
                    return uio::scancode::f5;
                case SDL_SCANCODE_F6:
                    return uio::scancode::f6;
                case SDL_SCANCODE_F7:
                    return uio::scancode::f7;
                case SDL_SCANCODE_F8:
                    return uio::scancode::f8;
                case SDL_SCANCODE_F9:
                    return uio::scancode::f9;
                case SDL_SCANCODE_F10:
                    return uio::scancode::f10;
                case SDL_SCANCODE_F11:
                    return uio::scancode::f11;
                case SDL_SCANCODE_F12:
                    return uio::scancode::f12;

                case SDL_SCANCODE_PRINTSCREEN:
                    return uio::scancode::print_screen;
                case SDL_SCANCODE_SCROLLLOCK:
                    return uio::scancode::scroll_lock;
                case SDL_SCANCODE_PAUSE:
                    return uio::scancode::pause;
                case SDL_SCANCODE_INSERT:
                    return uio::scancode::insert;
                case SDL_SCANCODE_HOME:
                    return uio::scancode::home;
                case SDL_SCANCODE_PAGEUP:
                    return uio::scancode::page_up;
                case SDL_SCANCODE_DELETE:
                    return uio::scancode::del;
                case SDL_SCANCODE_END:
                    return uio::scancode::end;
                case SDL_SCANCODE_PAGEDOWN:
                    return uio::scancode::page_down;
                case SDL_SCANCODE_RIGHT:
                    return uio::scancode::right;
                case SDL_SCANCODE_LEFT:
                    return uio::scancode::left;
                case SDL_SCANCODE_DOWN:
                    return uio::scancode::down;
                case SDL_SCANCODE_UP:
                    return uio::scancode::up;
                case SDL_SCANCODE_NUMLOCKCLEAR:
                    return uio::scancode::numlock_clear;

                case SDL_SCANCODE_KP_DIVIDE:
                    return uio::scancode::kp_divide;
                case SDL_SCANCODE_KP_MULTIPLY:
                    return uio::scancode::kp_multiply;
                case SDL_SCANCODE_KP_MINUS:
                    return uio::scancode::kp_minus;
                case SDL_SCANCODE_KP_PLUS:
                    return uio::scancode::kp_plus;
                case SDL_SCANCODE_KP_ENTER:
                    return uio::scancode::kp_enter;
                case SDL_SCANCODE_KP_1:
                    return uio::scancode::kp_1;
                case SDL_SCANCODE_KP_2:
                    return uio::scancode::kp_2;
                case SDL_SCANCODE_KP_3:
                    return uio::scancode::kp_3;
                case SDL_SCANCODE_KP_4:
                    return uio::scancode::kp_4;
                case SDL_SCANCODE_KP_5:
                    return uio::scancode::kp_5;
                case SDL_SCANCODE_KP_6:
                    return uio::scancode::kp_6;
                case SDL_SCANCODE_KP_7:
                    return uio::scancode::kp_7;
                case SDL_SCANCODE_KP_8:
                    return uio::scancode::kp_8;
                case SDL_SCANCODE_KP_9:
                    return uio::scancode::kp_9;
                case SDL_SCANCODE_KP_0:
                    return uio::scancode::kp_0;
                case SDL_SCANCODE_KP_PERIOD:
                    return uio::scancode::kp_period;

                case SDL_SCANCODE_APPLICATION:
                    return uio::scancode::application;
                case SDL_SCANCODE_POWER:
                    return uio::scancode::power;
                case SDL_SCANCODE_KP_EQUALS:
                    return uio::scancode::kp_equals;

                case SDL_SCANCODE_EXECUTE:
                    return uio::scancode::execute;
                case SDL_SCANCODE_HELP:
                    return uio::scancode::help;
                case SDL_SCANCODE_MENU:
                    return uio::scancode::menu;
                case SDL_SCANCODE_SELECT:
                    return uio::scancode::select;
                case SDL_SCANCODE_STOP:
                    return uio::scancode::stop;
                case SDL_SCANCODE_AGAIN:
                    return uio::scancode::again;
                case SDL_SCANCODE_UNDO:
                    return uio::scancode::undo;
                case SDL_SCANCODE_CUT:
                    return uio::scancode::cut;
                case SDL_SCANCODE_COPY:
                    return uio::scancode::copy;
                case SDL_SCANCODE_PASTE:
                    return uio::scancode::paste;
                case SDL_SCANCODE_FIND:
                    return uio::scancode::find;
                case SDL_SCANCODE_MUTE:
                    return uio::scancode::mute;
                case SDL_SCANCODE_VOLUMEUP:
                    return uio::scancode::volumeup;
                case SDL_SCANCODE_VOLUMEDOWN:
                    return uio::scancode::volumedown;

                case SDL_SCANCODE_LCTRL:
                    return uio::scancode::lctrl;
                case SDL_SCANCODE_LSHIFT:
                    return uio::scancode::lshift;
                case SDL_SCANCODE_LALT:
                    return uio::scancode::lalt;
                case SDL_SCANCODE_LGUI:
                    return uio::scancode::lgui;
                case SDL_SCANCODE_RCTRL:
                    return uio::scancode::rctrl;
                case SDL_SCANCODE_RSHIFT:
                    return uio::scancode::rshift;
                case SDL_SCANCODE_RALT:
                    return uio::scancode::ralt;
                case SDL_SCANCODE_RGUI:
                    return uio::scancode::rgui;

                case SDL_SCANCODE_UNKNOWN:
                default:
                    return uio::scancode::unknown;
            }
        }

        static SDL_Keycode cast_keycode(uio::keycode key) {
            switch(key) {
                case uio::keycode::enter:
                    return SDLK_RETURN;
                case uio::keycode::escape:
                    return SDLK_ESCAPE;
                case uio::keycode::backspace:
                    return SDLK_BACKSPACE;
                case uio::keycode::tab:
                    return SDLK_TAB;
                case uio::keycode::space:
                    return SDLK_SPACE;
                case uio::keycode::exclaimation:
                    return SDLK_EXCLAIM;
                case uio::keycode::doublequote:
                    return SDLK_QUOTEDBL;
                case uio::keycode::hash:
                    return SDLK_HASH;
                case uio::keycode::percent:
                    return SDLK_PERCENT;
                case uio::keycode::dollar:
                    return SDLK_DOLLAR;
                case uio::keycode::ampersand:
                    return SDLK_AMPERSAND;
                case uio::keycode::quote:
                    return SDLK_QUOTE;
                case uio::keycode::parentesis_left:
                    return SDLK_LEFTPAREN;
                case uio::keycode::parentesis_right:
                    return SDLK_RIGHTPAREN;
                case uio::keycode::asterisk:
                    return SDLK_ASTERISK;
                case uio::keycode::plus:
                    return SDLK_PLUS;
                case uio::keycode::comma:
                    return SDLK_COMMA;
                case uio::keycode::minus:
                    return SDLK_MINUS;
                case uio::keycode::period:
                    return SDLK_PERIOD;
                case uio::keycode::slash:
                    return SDLK_SLASH;
                case uio::keycode::num_0:
                    return SDLK_0;
                case uio::keycode::num_1:
                    return SDLK_1;
                case uio::keycode::num_2:
                    return SDLK_2;
                case uio::keycode::num_3:
                    return SDLK_3;
                case uio::keycode::num_4:
                    return SDLK_4;
                case uio::keycode::num_5:
                    return SDLK_5;
                case uio::keycode::num_6:
                    return SDLK_6;
                case uio::keycode::num_7:
                    return SDLK_7;
                case uio::keycode::num_8:
                    return SDLK_8;
                case uio::keycode::num_9:
                    return SDLK_9;
                case uio::keycode::colon:
                    return SDLK_COLON;
                case uio::keycode::semicolon:
                    return SDLK_SEMICOLON;
                case uio::keycode::less:
                    return SDLK_LESS;
                case uio::keycode::equal:
                    return SDLK_EQUALS;
                case uio::keycode::greater:
                    return SDLK_GREATER;
                case uio::keycode::question:
                    return SDLK_QUESTION;
                case uio::keycode::at:
                    return SDLK_AT;
                case uio::keycode::bracket_left:
                    return SDLK_LEFTBRACKET;
                case uio::keycode::backslash:
                    return SDLK_BACKSLASH;
                case uio::keycode::bracket_right:
                    return SDLK_RIGHTBRACKET;
                case uio::keycode::caret:
                    return SDLK_CARET;
                case uio::keycode::underscore:
                    return SDLK_UNDERSCORE;
                case uio::keycode::backquote:
                    return SDLK_BACKQUOTE;
                case uio::keycode::a:
                    return SDLK_a;
                case uio::keycode::b:
                    return SDLK_b;
                case uio::keycode::c:
                    return SDLK_c;
                case uio::keycode::d:
                    return SDLK_d;
                case uio::keycode::e:
                    return SDLK_e;
                case uio::keycode::f:
                    return SDLK_f;
                case uio::keycode::g:
                    return SDLK_g;
                case uio::keycode::h:
                    return SDLK_h;
                case uio::keycode::i:
                    return SDLK_i;
                case uio::keycode::j:
                    return SDLK_j;
                case uio::keycode::k:
                    return SDLK_k;
                case uio::keycode::l:
                    return SDLK_l;
                case uio::keycode::m:
                    return SDLK_m;
                case uio::keycode::n:
                    return SDLK_n;
                case uio::keycode::o:
                    return SDLK_o;
                case uio::keycode::p:
                    return SDLK_p;
                case uio::keycode::q:
                    return SDLK_q;
                case uio::keycode::r:
                    return SDLK_r;
                case uio::keycode::s:
                    return SDLK_s;
                case uio::keycode::t:
                    return SDLK_t;
                case uio::keycode::u:
                    return SDLK_u;
                case uio::keycode::v:
                    return SDLK_v;
                case uio::keycode::w:
                    return SDLK_w;
                case uio::keycode::x:
                    return SDLK_x;
                case uio::keycode::y:
                    return SDLK_y;
                case uio::keycode::z:
                    return SDLK_z;

                case uio::keycode::unknown:
                default:
                    return SDLK_UNKNOWN;
            }
        }

        static SDL_Scancode cast_scancode(uio::scancode code) {
            switch(code) {
                case uio::scancode::a:
                    return SDL_SCANCODE_A;
                case uio::scancode::b:
                    return SDL_SCANCODE_B;
                case uio::scancode::c:
                    return SDL_SCANCODE_C;
                case uio::scancode::d:
                    return SDL_SCANCODE_D;
                case uio::scancode::e:
                    return SDL_SCANCODE_E;
                case uio::scancode::f:
                    return SDL_SCANCODE_F;
                case uio::scancode::g:
                    return SDL_SCANCODE_G;
                case uio::scancode::h:
                    return SDL_SCANCODE_H;
                case uio::scancode::i:
                    return SDL_SCANCODE_I;
                case uio::scancode::j:
                    return SDL_SCANCODE_J;
                case uio::scancode::k:
                    return SDL_SCANCODE_K;
                case uio::scancode::l:
                    return SDL_SCANCODE_L;
                case uio::scancode::m:
                    return SDL_SCANCODE_M;
                case uio::scancode::n:
                    return SDL_SCANCODE_N;
                case uio::scancode::o:
                    return SDL_SCANCODE_O;
                case uio::scancode::p:
                    return SDL_SCANCODE_P;
                case uio::scancode::q:
                    return SDL_SCANCODE_Q;
                case uio::scancode::r:
                    return SDL_SCANCODE_R;
                case uio::scancode::s:
                    return SDL_SCANCODE_S;
                case uio::scancode::t:
                    return SDL_SCANCODE_T;
                case uio::scancode::u:
                    return SDL_SCANCODE_U;
                case uio::scancode::v:
                    return SDL_SCANCODE_V;
                case uio::scancode::w:
                    return SDL_SCANCODE_W;
                case uio::scancode::x:
                    return SDL_SCANCODE_X;
                case uio::scancode::y:
                    return SDL_SCANCODE_Y;
                case uio::scancode::z:
                    return SDL_SCANCODE_Z;

                case uio::scancode::num_1:
                    return SDL_SCANCODE_1;
                case uio::scancode::num_2:
                    return SDL_SCANCODE_2;
                case uio::scancode::num_3:
                    return SDL_SCANCODE_3;
                case uio::scancode::num_4:
                    return SDL_SCANCODE_4;
                case uio::scancode::num_5:
                    return SDL_SCANCODE_5;
                case uio::scancode::num_6:
                    return SDL_SCANCODE_6;
                case uio::scancode::num_7:
                    return SDL_SCANCODE_7;
                case uio::scancode::num_8:
                    return SDL_SCANCODE_8;
                case uio::scancode::num_9:
                    return SDL_SCANCODE_9;
                case uio::scancode::num_0:
                    return SDL_SCANCODE_0;

                case uio::scancode::enter:
                    return SDL_SCANCODE_RETURN;
                case uio::scancode::escape:
                    return SDL_SCANCODE_ESCAPE;
                case uio::scancode::backspace:
                    return SDL_SCANCODE_BACKSPACE;
                case uio::scancode::tab:
                    return SDL_SCANCODE_TAB;
                case uio::scancode::space:
                    return SDL_SCANCODE_SPACE;

                case uio::scancode::minus:
                    return SDL_SCANCODE_MINUS;
                case uio::scancode::equals:
                    return SDL_SCANCODE_EQUALS;
                case uio::scancode::bracket_left:
                    return SDL_SCANCODE_LEFTBRACKET;
                case uio::scancode::bracket_right:
                    return SDL_SCANCODE_RIGHTBRACKET;
                case uio::scancode::backslash:
                    return SDL_SCANCODE_BACKSLASH;

                case uio::scancode::semicolon:
                    return SDL_SCANCODE_SEMICOLON;
                case uio::scancode::apostrophe:
                    return SDL_SCANCODE_APOSTROPHE;
                case uio::scancode::grave:
                    return SDL_SCANCODE_GRAVE;
                case uio::scancode::comma:
                    return SDL_SCANCODE_COMMA;
                case uio::scancode::period:
                    return SDL_SCANCODE_PERIOD;
                case uio::scancode::slash:
                    return SDL_SCANCODE_SLASH;

                case uio::scancode::caps_lock:
                    return SDL_SCANCODE_CAPSLOCK;

                case uio::scancode::f1:
                    return SDL_SCANCODE_F1;
                case uio::scancode::f2:
                    return SDL_SCANCODE_F2;
                case uio::scancode::f3:
                    return SDL_SCANCODE_F3;
                case uio::scancode::f4:
                    return SDL_SCANCODE_F4;
                case uio::scancode::f5:
                    return SDL_SCANCODE_F5;
                case uio::scancode::f6:
                    return SDL_SCANCODE_F6;
                case uio::scancode::f7:
                    return SDL_SCANCODE_F7;
                case uio::scancode::f8:
                    return SDL_SCANCODE_F8;
                case uio::scancode::f9:
                    return SDL_SCANCODE_F9;
                case uio::scancode::f10:
                    return SDL_SCANCODE_F10;
                case uio::scancode::f11:
                    return SDL_SCANCODE_F11;
                case uio::scancode::f12:
                    return SDL_SCANCODE_F12;

                case uio::scancode::print_screen:
                    return SDL_SCANCODE_PRINTSCREEN;
                case uio::scancode::scroll_lock:
                    return SDL_SCANCODE_SCROLLLOCK;
                case uio::scancode::pause:
                    return SDL_SCANCODE_PAUSE;
                case uio::scancode::insert:
                    return SDL_SCANCODE_INSERT;
                case uio::scancode::home:
                    return SDL_SCANCODE_HOME;
                case uio::scancode::page_up:
                    return SDL_SCANCODE_PAGEUP;
                case uio::scancode::del:
                    return SDL_SCANCODE_DELETE;
                case uio::scancode::end:
                    return SDL_SCANCODE_END;
                case uio::scancode::page_down:
                    return SDL_SCANCODE_PAGEDOWN;
                case uio::scancode::right:
                    return SDL_SCANCODE_RIGHT;
                case uio::scancode::left:
                    return SDL_SCANCODE_LEFT;
                case uio::scancode::down:
                    return SDL_SCANCODE_DOWN;
                case uio::scancode::up:
                    return SDL_SCANCODE_UP;
                case uio::scancode::numlock_clear:
                    return SDL_SCANCODE_NUMLOCKCLEAR;

                case uio::scancode::kp_divide:
                    return SDL_SCANCODE_KP_DIVIDE;
                case uio::scancode::kp_multiply:
                    return SDL_SCANCODE_KP_MULTIPLY;
                case uio::scancode::kp_minus:
                    return SDL_SCANCODE_KP_MINUS;
                case uio::scancode::kp_plus:
                    return SDL_SCANCODE_KP_PLUS;
                case uio::scancode::kp_enter:
                    return SDL_SCANCODE_KP_ENTER;
                case uio::scancode::kp_1:
                    return SDL_SCANCODE_KP_1;
                case uio::scancode::kp_2:
                    return SDL_SCANCODE_KP_2;
                case uio::scancode::kp_3:
                    return SDL_SCANCODE_KP_3;
                case uio::scancode::kp_4:
                    return SDL_SCANCODE_KP_4;
                case uio::scancode::kp_5:
                    return SDL_SCANCODE_KP_5;
                case uio::scancode::kp_6:
                    return SDL_SCANCODE_KP_6;
                case uio::scancode::kp_7:
                    return SDL_SCANCODE_KP_7;
                case uio::scancode::kp_8:
                    return SDL_SCANCODE_KP_8;
                case uio::scancode::kp_9:
                    return SDL_SCANCODE_KP_9;
                case uio::scancode::kp_0:
                    return SDL_SCANCODE_KP_0;
                case uio::scancode::kp_period:
                    return SDL_SCANCODE_KP_PERIOD;

                case uio::scancode::application:
                    return SDL_SCANCODE_APPLICATION;
                case uio::scancode::power:
                    return SDL_SCANCODE_POWER;
                case uio::scancode::kp_equals:
                    return SDL_SCANCODE_KP_EQUALS;

                case uio::scancode::execute:
                    return SDL_SCANCODE_EXECUTE;
                case uio::scancode::help:
                    return SDL_SCANCODE_HELP;
                case uio::scancode::menu:
                    return SDL_SCANCODE_MENU;
                case uio::scancode::select:
                    return SDL_SCANCODE_SELECT;
                case uio::scancode::stop:
                    return SDL_SCANCODE_STOP;
                case uio::scancode::again:
                    return SDL_SCANCODE_AGAIN;
                case uio::scancode::undo:
                    return SDL_SCANCODE_UNDO;
                case uio::scancode::cut:
                    return SDL_SCANCODE_CUT;
                case uio::scancode::copy:
                    return SDL_SCANCODE_COPY;
                case uio::scancode::paste:
                    return SDL_SCANCODE_PASTE;
                case uio::scancode::find:
                    return SDL_SCANCODE_FIND;
                case uio::scancode::mute:
                    return SDL_SCANCODE_MUTE;
                case uio::scancode::volumeup:
                    return SDL_SCANCODE_VOLUMEUP;
                case uio::scancode::volumedown:
                    return SDL_SCANCODE_VOLUMEDOWN;

                case uio::scancode::lctrl:
                    return SDL_SCANCODE_LCTRL;
                case uio::scancode::lshift:
                    return SDL_SCANCODE_LSHIFT;
                case uio::scancode::lalt:
                    return SDL_SCANCODE_LALT;
                case uio::scancode::lgui:
                    return SDL_SCANCODE_LGUI;
                case uio::scancode::rctrl:
                    return SDL_SCANCODE_RCTRL;
                case uio::scancode::rshift:
                    return SDL_SCANCODE_RSHIFT;
                case uio::scancode::ralt:
                    return SDL_SCANCODE_RALT;
                case uio::scancode::rgui:
                    return SDL_SCANCODE_RGUI;

                case uio::scancode::unknown:
                default:
                    return SDL_SCANCODE_UNKNOWN;
            }
        }

        static uio::keymods cast_keymods(uint16_t mod) {
            uio::keymods modifiers;

            if (has_some_flags(KMOD_CTRL, mod)) {
                modifiers.set(uio::keymod::ctrl);
            }

            if (has_some_flags(KMOD_ALT, mod)) {
                modifiers.set(uio::keymod::alt);
            }

            if (has_some_flags(KMOD_SHIFT, mod)) {
                modifiers.set(uio::keymod::shift);
            }

            if (has_some_flags(KMOD_GUI, mod)) {
                modifiers.set(uio::keymod::cmd);
            }

            return modifiers;
        }

        keyboard::keyboard(sdl::app & app, sdl::window & w, const uio::keyboard_input::allocator_type & alloc) :
            uio::keyboard_input(alloc),
            _window(w),
            _event_observer(app.on_event, {[this](const auto & e) {
                switch (e.type) {
                    case SDL_KEYDOWN: {
                        if (_window.id() != e.key.windowID) {
                            return;
                        }

                        press(e.key.keysym.sym, e.key.keysym.scancode, e.key.keysym.mod);
                        break;
                    }

                    case SDL_KEYUP: {
                        if (_window.id() != e.key.windowID) {
                            return;
                        }

                        release(e.key.keysym.sym, e.key.keysym.scancode, e.key.keysym.mod);
                        break;
                    }
                }
            }})
        {}

        bool keyboard::is_pressed(uio::keycode keycode) const {
            auto state = SDL_GetKeyboardState(nullptr);
            return state[SDL_GetScancodeFromKey(cast_keycode(keycode))] == 1;
        }

        bool keyboard::is_pressed(uio::scancode scancode) const {
            auto state = SDL_GetKeyboardState(nullptr);
            return state[cast_scancode(scancode)] == 1;
        }

        uio::keymods keyboard::modifiers() const {
            return cast_keymods(SDL_GetModState());
        }

        void keyboard::press(SDL_Keycode key, SDL_Scancode scancode, uint16_t modifiers) {
            process_press(uio::keyboard_event{cast_keycode(key), cast_scancode(scancode), cast_keymods(modifiers), *this});
        }

        void keyboard::release(SDL_Keycode key, SDL_Scancode scancode, uint16_t modifiers) {
            process_release(uio::keyboard_event{cast_keycode(key), cast_scancode(scancode), cast_keymods(modifiers), *this});
        }
    }
}

//---------------------------------------------------------------------------
