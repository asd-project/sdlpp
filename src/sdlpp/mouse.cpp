//---------------------------------------------------------------------------

#include <sdlpp/mouse.h>
#include <SDL.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        static std::optional<uio::mouse_button> cast_mouse_button(uint8 button) {
            switch(button) {
                case SDL_BUTTON_LEFT:
                    return uio::mouse_button::left;
                case SDL_BUTTON_RIGHT:
                    return uio::mouse_button::right;
                case SDL_BUTTON_MIDDLE:
                    return uio::mouse_button::middle;
                case SDL_BUTTON_X1:
                    return uio::mouse_button::x1;
                case SDL_BUTTON_X2:
                    return uio::mouse_button::x2;
                default:
                    return {};
            }
        }

        mouse::mouse(sdl::app & app, sdl::window & w, const uio::mouse_input::allocator_type & alloc) :
            uio::mouse_input(alloc),
            _window(w),
            _event_observer(app.on_event, {[this](const auto & e) {
                if (!_window.active()) {
                    return;
                }

                switch (e.type) {
                    case SDL_MOUSEMOTION: {
                        if (SDL_GetRelativeMouseMode() == SDL_TRUE) {
                            move_by({e.motion.xrel, e.motion.yrel});
                        } else {
                            move({e.motion.x, e.motion.y});
                        }

                        break;
                    }

                    case SDL_MOUSEBUTTONDOWN: {
                        // spdlog::debug("mouse down: {} {}, which: {}", e.button.x, e.button.y, e.button.which);
                        press({e.button.x, e.button.y}, e.button.button);
                        break;
                    }

                    case SDL_MOUSEBUTTONUP: {
                        // spdlog::debug("mouse up: {} {}, which: {}", e.button.x, e.button.y, e.button.which);
                        release({e.button.x, e.button.y}, e.button.button);
                        break;
                    }

                    case SDL_FINGERDOWN: {
                        // spdlog::debug("finger down: {} {}, which: {}", e.tfinger.x, e.tfinger.y, e.tfinger.touchId);
                        touch_start({e.tfinger.x, e.tfinger.y}, SDL_GetNumTouchFingers(e.tfinger.touchId));
                        break;
                    }

                    case SDL_FINGERUP: {
                        // spdlog::debug("finger up: {} {}, which: {}", e.tfinger.x, e.tfinger.y, e.tfinger.touchId);
                        touch_end({e.tfinger.x, e.tfinger.y}, SDL_GetNumTouchFingers(e.tfinger.touchId));
                        break;
                    }

                    case SDL_FINGERMOTION: {
                        constexpr int scroll_fingers_count = 2;

                        auto num = SDL_GetNumTouchFingers(e.tfinger.touchId);

                        if (!is_pressed() && num == scroll_fingers_count) {
                            wheel({e.tfinger.dx * 64, e.tfinger.dy * 64});
#if BOOST_OS_MACOS
                            _last_touch_scroll_timestamp = e.tfinger.timestamp;
#endif
                        }

                        break;
                    }

                    case SDL_MOUSEWHEEL: {
                        if (e.wheel.which == SDL_TOUCH_MOUSEID) {
                            break;
                        }

#if BOOST_OS_MACOS
                        // avoid duplicate touch bar scroll
                        if (math::abs(i64(e.wheel.timestamp) - _last_touch_scroll_timestamp) < 100) {
                            break;
                        }

                        wheel({-e.wheel.x, e.wheel.y});
#else
                        if (e.wheel.direction == SDL_MOUSEWHEEL_FLIPPED) {
                            wheel({-e.wheel.x, -e.wheel.y});
                        } else {
                            wheel({e.wheel.x, e.wheel.y});
                        }
#endif

                        break;
                    }
                }
            }})
        {}

        math::float_point mouse::position() const {
            return _position;
        }

        uio::mouse_buttons mouse::buttons() const {
            return _buttons;
        }

        void mouse::move(const math::float_point & pos) {
            if (_position == pos) {
                return;
            }

            auto delta = pos - _position;

            _position = pos;
            this->process_move(uio::mouse_move_event{delta, *this});
        }

        void mouse::move_by(const math::float_point & delta) {
            this->process_move(uio::mouse_move_event{delta, *this});
        }

        void mouse::press(const math::float_point & pos, uint8 b) {
            auto button = cast_mouse_button(b);

            if (!button || _buttons.check(*button)) {
                return;
            }

            _buttons.set(*button);
            this->process_press(uio::mouse_button_event{*button, *this});
        }

        void mouse::release(const math::float_point & pos, uint8 b) {
            auto button = cast_mouse_button(b);

            if (!button || !_buttons.check(*button)) {
                return;
            }

            _buttons.clear(*button);
            this->process_release(uio::mouse_button_event{*button, *this});
        }

        void mouse::wheel(const math::float_point & delta) {
            this->process_wheel(uio::mouse_wheel_event{delta, *this});
        }

        void mouse::touch_start(const math::float_point & pos, int touch_count) {
            this->process_touch_start(uio::touch_event{*this, touch_count});
        }

        void mouse::touch_end(const math::float_point & pos, int touch_count) {
            this->process_touch_end(uio::touch_event{*this, touch_count});
        }

        void mouse::set_position(const math::float_point & pos) {
            SDL_WarpMouseInWindow(_window, static_cast<int>(pos.x), static_cast<int>(pos.y));
        }

        void mouse::reset_position() {
            const auto & window_size = _window.logical_size();
            SDL_WarpMouseInWindow(_window, static_cast<int>(window_size.x / 2), static_cast<int>(window_size.y / 2));
        }

        void mouse::set_relative_mode(bool enabled) {
            SDL_SetRelativeMouseMode(enabled ? SDL_TRUE : SDL_FALSE);
        }

        void mouse::force_update() {
            math::int_point pos;
            SDL_GetMouseState(&pos.x, &pos.y);

            if (pos != math::floor<int>(_position)) {
                move(math::float_point(pos));
            }
        }
    }
}

//---------------------------------------------------------------------------
