//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <uio/keyboard.h>
#include <SDL.h>

#include <sdlpp/window.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        class keyboard : public uio::keyboard_input
        {
        public:
            export_api(sdlpp)
            explicit keyboard(sdl::app & app, sdl::window & window, const uio::keyboard_input::allocator_type & alloc = {});

            export_api(sdlpp)
            virtual bool is_pressed(uio::keycode keycode) const override;
            export_api(sdlpp)
            virtual bool is_pressed(uio::scancode scancode) const override;
            export_api(sdlpp)
            virtual uio::keymods modifiers() const override;

            export_api(sdlpp)
            void press(SDL_Keycode key, SDL_Scancode scancode, uint16_t modifiers);
            export_api(sdlpp)
            void release(SDL_Keycode key, SDL_Scancode scancode, uint16_t modifiers);

        private:
            sdl::window & _window;
            signal::observer<void(const SDL_Event & e)> _event_observer;
        };
    }
}
