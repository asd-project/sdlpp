//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#ifndef HAVE_M_PI
#define HAVE_M_PI
#endif

#include <SDL.h>

#include <meta/common.h>
#include <container/container.h>

#include <signal/member_source.h>

#include <boost/predef.h>

#include <sdlpp/exception.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        class window;

        struct app_options
        {
            uint32 flags = SDL_INIT_VIDEO | SDL_INIT_EVENTS;
        };

        class app
        {
        public:
            using allocator_type = std::pmr::polymorphic_allocator<app>;

            export_api(sdlpp)
            app(app_options options = {}, const allocator_type & alloc = {});
            app(const app &) = delete;

            export_api(sdlpp)
            ~app();

            app & operator = (const app &) = delete;

            // return false if need to exit
            export_api(sdlpp)
            bool process_events();

            export_api(sdlpp)
            void quit();

            export_api(sdlpp)
            sdl::window * active_window() const;

            void register_window(sdl::window * window) {
                _windows.push_back(window);
            }

            void unregister_window(sdl::window * window) {
                container::try_erase(_windows, window);
            }

            signal::member_source<app, void(const SDL_Event &)> on_event;

        private:
            bool _need_close = false;

            std::pmr::vector<sdl::window *> _windows;

            SDL_Event _event;
        };
    }
}
