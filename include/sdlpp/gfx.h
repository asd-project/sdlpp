//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <sdlpp/window.h>
#include <opengl/opengl.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        class glew_exception : public std::runtime_error
        {
        public:
            glew_exception(GLenum error, const std::string & desc) :
                std::runtime_error(desc + " GLEW error: " + reinterpret_cast<const char *>(glewGetErrorString(error))) {}
        };

        class surface
        {
        public:
            surface(sdl::window & window) {
                _handle = SDL_GetWindowSurface(window);

                if (_handle == nullptr) {
                    BOOST_THROW_EXCEPTION(exception("Couldn't create window surface!"));
                }
            }

            surface(SDL_Surface * handle) : _handle(handle) {}

            surface(surface && s) : _handle(s._handle) {
                s._handle = nullptr;
            }

            surface & operator =(surface && s) {
                std::swap(_handle, s._handle);
                return *this;
            }

            ~surface() {
                if (_handle != nullptr) {
                    SDL_FreeSurface(_handle);
                }
            }

            bool operator ==(SDL_Surface * h) const {
                return _handle == h;
            }

            bool operator !=(SDL_Surface * h) const {
                return _handle != h;
            }

            operator SDL_Surface *() {
                return _handle;
            }

            SDL_Surface * operator ->() {
                return _handle;
            }

        private:
            SDL_Surface * _handle = nullptr;
        };

        class graphics : public opengl::graphics
        {
        public:
            export_api(sdlpp)
            graphics(sdl::window & window, const opengl::configuration & config = {});

            virtual ~graphics() {
                if (_context) {
                    SDL_GL_DeleteContext(_context);
                }
            }

            graphics(graphics && ctx) noexcept :
                opengl::graphics(ctx),
                _context(ctx._context),
                _window(ctx._window)
            {
                ctx._context = 0;
            }

            graphics & operator = (graphics && ctx) noexcept {
                std::swap(_context, ctx._context);
                return *this;
            }

            bool operator == (const graphics & ctx) const {
                return this == &ctx;
            }

            void set_viewport(const math::int_rect & rect) {
                glViewport(rect.left, rect.top, rect.width(), rect.height());
            }

            void present() {
#if !ASD_PLAT_EMSCRIPTEN
                glFinish();
#endif
                SDL_GL_SwapWindow(_window);
            }

            SDL_GLContext native_handle() {
                return _context;
            }

            operator SDL_GLContext() {
                return _context;
            }

        private:
            SDL_GLContext _context;
            sdl::window & _window;
        };
    }
}
