//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <boost/di.hpp>

#include <opengl/uniform.h>
#include <opengl/shader.h>
#include <opengl/mesh.h>
#include <opengl/multimesh.h>
#include <opengl/render_target.h>
#include <opengl/render_pass.h>
#include <opengl/pipeline.h>

#include <sdlpp/gfx.h>
#include <sdlpp/mouse.h>
#include <sdlpp/keyboard.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        inline auto io_module() {
            namespace di = boost::di;

            return di::make_injector(
                di::bind<uio::mouse_input>().in(di::singleton).to<sdl::mouse>(),
                di::bind<uio::mouse_output>().in(di::singleton).to<sdl::mouse>(),
                di::bind<uio::keyboard_input>().in(di::singleton).to<sdl::keyboard>()
            );
        }

        inline auto gfx_module() {
            namespace di = boost::di;

            return di::make_injector(
                di::bind<opengl::graphics>().in(di::singleton).to<sdl::graphics>()
            );
        }

        inline auto module() {
            namespace di = boost::di;

            return di::make_injector(io_module(), gfx_module());
        }
    }
}
