//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/rect.h>
#include <sdlpp/app.h>

#include <signal/member_source.h>
#include <signal/observer.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        using window_size = math::float_size;

        struct window_title
        {
            window_title(const char * value) : value(value) {}
            window_title(std::string value) : value(std::move(value)) {}

            std::string value;

            operator const std::string & () const {
                return value;
            }
        };

        class window
        {
        public:
            export_api(sdlpp)
            window(sdl::app & app, const sdl::window_title & title, const window_size & size);

            window(const window &) = delete;

            export_api(sdlpp)
            ~window();

            window & operator=(const window &) = delete;

            export_api(sdlpp)
            uint32 id() const;

            export_api(sdlpp)
            bool active() const;

            const window_size & logical_size() const {
                return _logical_size;
            }

            const math::int_size & physical_size() const {
                return _physical_size;
            }

            math::float_size render_scale() const {
                return math::float_size(_physical_size) / _logical_size;
            }

            operator SDL_Window * () {
                return _handle;
            }

            SDL_Window * operator -> () {
                return _handle;
            }

            export_api(sdlpp)
            void resize(const window_size & size);

            export_api(sdlpp)
            void process_event(const SDL_WindowEvent & event);

            signal::member_source<window, void(const window_size & logical, const math::int_size & physical)> on_resize;

        protected:
            sdl::app & _app;
            SDL_Window * _handle = nullptr;

            window_size _logical_size;
            math::int_size _physical_size;
            signal::observer<void(const SDL_Event & e)> _event_observer;
        };

        inline math::float_size get_dpi(int display = 0) noexcept {
            math::float_size dpi{72, 72};
            SDL_GetDisplayDPI(display, nullptr, &dpi.x, &dpi.y);

            return dpi;
        }
    }
}
