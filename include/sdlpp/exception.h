//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <stdexcept>
#include <string>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        class exception : public std::runtime_error
        {
        public:
            exception(const std::string & desc) : std::runtime_error(desc + " SDL error: " + SDL_GetError()) {}
        };

    }
}
