//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <uio/mouse.h>
#include <sdlpp/window.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace sdl
    {
        class mouse : public uio::mouse_input, public uio::mouse_output
        {
        public:
            export_api(sdlpp)
            explicit mouse(sdl::app & app, sdl::window & window, const uio::mouse_input::allocator_type & alloc = {});

            export_api(sdlpp)
            math::float_point position() const override;
            export_api(sdlpp)
            uio::mouse_buttons buttons() const override;

            export_api(sdlpp)
            void set_position(const math::float_point & pos) override;
            export_api(sdlpp)
            void reset_position() override;
            export_api(sdlpp)
            void set_relative_mode(bool enabled) override;

            export_api(sdlpp)
            void move(const math::float_point & pos);
            export_api(sdlpp)
            void move_by(const math::float_point & delta);
            export_api(sdlpp)
            void press(const math::float_point & pos, uint8 button);
            export_api(sdlpp)
            void release(const math::float_point & pos, uint8 button);
            export_api(sdlpp)
            void wheel(const math::float_point & delta);
            export_api(sdlpp)
            void touch_start(const math::float_point & pos, int touch_count);
            export_api(sdlpp)
            void touch_end(const math::float_point & pos, int touch_count);

            export_api(sdlpp)
            void force_update();

        private:
            math::float_point _position;
            uio::mouse_buttons _buttons;

            sdl::window & _window;
            signal::observer<void(const SDL_Event & e)> _event_observer;

#if BOOST_OS_MACOS
            u32 _last_touch_scroll_timestamp = 0;
#endif
        };
    }
}
