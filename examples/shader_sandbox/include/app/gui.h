//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <opengl/opengl.h>
#include <opengl/uniform.h>
#include <opengl/shader.h>
#include <opengl/mesh.h>
#include <opengl/render_target.h>
#include <opengl/render_pass.h>
#include <opengl/pipeline.h>

#include <sdlpp/module.h>

#define IM_VEC2_CLASS_EXTRA                                                                         \
    ImVec2(const ::asd::math::float_point & f) { x = f.x; y = f.y; }                                \
    ImVec2(const ::asd::math::float_size & f) { x = f.x; y = f.y; }                                 \
    operator ::asd::math::float_point() const { return ::asd::math::float_point(x, y); }            \
    operator ::asd::math::float_size() const { return ::asd::math::float_size(x, y); }

#define IM_VEC4_CLASS_EXTRA                                                                         \
    ImVec4(const ::asd::math::float_vector & f) { x = f.x; y = f.y; z = f.z; w = f.w; }             \
    operator ::asd::math::float_vector() const { return ::asd::math::float_vector(x, y, z, w); }

#include <imgui.h>
#include <imgui_internal.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_sdl.h>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/base_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <boost/algorithm/string.hpp>

#include <fs/fs.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        template <class Mutex>
        class buffer_sink : public spdlog::sinks::base_sink<Mutex>
        {
        public:
            const auto & lines() const {
                return _lines;
            }

        protected:
            void sink_it_(const spdlog::details::log_msg & msg) override {
                spdlog::memory_buf_t formatted;
                spdlog::sinks::base_sink<Mutex>::formatter_->format(msg, formatted);

                auto str = fmt::to_string(formatted);
                boost::trim_if(str, boost::is_any_of(" \t\n"));
                boost::split(_buffer, str, boost::is_any_of("\n"));

                if (_lines.size() + _buffer.size() > _max_lines_count) {
                    _lines.erase(_lines.begin(), _lines.begin() + _lines.size() + _buffer.size() - _max_lines_count);
                }

                _lines.insert(_lines.end(), _buffer.begin(), _buffer.end());
            }

            void flush_() override {}

        private:
            size_t _max_lines_count = 100;
            std::vector<std::string> _buffer;
            std::vector<std::string> _lines;
        };

        using buffer_sink_mt = buffer_sink<std::mutex>;
        using buffer_sink_st = buffer_sink<spdlog::details::null_mutex>;

        inline const char * c_str(const char * s) {
            return s;
        }

        inline const char * c_str(const std::string & s) {
            return s.c_str();
        }

        struct gui
        {
            sdl::app &      app;
            sdl::window &   window;
            sdl::graphics & graphics;

            std::shared_ptr<buffer_sink_st> log_buffer;
            ImFont * log_font;
            signal::hub hub;

            gui(sdl::app & app, sdl::window & window, sdl::graphics & graphics, const opengl::configuration & opengl_config) :
                app(app), window(window), graphics(graphics),
                log_buffer(std::make_shared<buffer_sink_st>())
            {
                log_buffer->set_pattern("[%^%l%$] %v");
                auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
                spdlog::set_default_logger(std::make_shared<spdlog::logger>("default", spdlog::sinks_init_list{console_sink, log_buffer}));

                IMGUI_CHECKVERSION();
                ImGui::CreateContext();

                ImGuiIO & imgui_io = ImGui::GetIO();
#if !ASD_PLAT_EMSCRIPTEN
                imgui_io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
#endif
                imgui_io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;

                auto font_path = application::root_path() / "fonts/Roboto-Medium.ttf";

                ImFontConfig config;
                config.OversampleH = 3;
                config.OversampleV = 1;
                log_font = imgui_io.Fonts->AddFontFromFileTTF(font_path.string().c_str(), 12.0f, &config);

                auto version_string = opengl::graphics::create_version_string(opengl_config);

                ImGui_ImplSDL2_InitForOpenGL(window, graphics);
                ImGui_ImplOpenGL3_Init(version_string.c_str());

                ImGui::StyleColorsDark();
                ImGuiStyle & style = ImGui::GetStyle();
                style.WindowRounding = 0.0f;
                style.Colors[ImGuiCol_WindowBg].w = 1.0f;

                hub.subscribe(app.on_event, [&](const SDL_Event & e) {
                    ImGui_ImplSDL2_ProcessEvent(&e);
                });
            }

            ~gui() {
                ImGui_ImplOpenGL3_Shutdown();
                ImGui_ImplSDL2_Shutdown();
                ImGui::DestroyContext();
            }

            template <class F>
            void update(F custom_update) {
                ImGui_ImplOpenGL3_NewFrame();
                ImGui_ImplSDL2_NewFrame(window);

                ImGui::NewFrame();

                auto dockspace_id = ImGui::DockSpaceOverViewport(ImGui::GetMainViewport(), ImGuiDockNodeFlags_PassthruCentralNode);

                if (ImGui::BeginMainMenuBar()) {
                    if (ImGui::BeginMenu("File")) {
                        // if (ImGui::MenuItem(gui.config.fullscreen ? "Leave fullscreen" : "Enter fullscreen", "Alt+Enter")) {
                            // gui.set_fullscreen(!gui.config.fullscreen);
                        // }

                        // if (ImGui::MenuItem("Close tool window", "Ctrl+W")) {
                            // gui.config.tool_active = false;
                        // }

                        if (ImGui::MenuItem("Close app", "Ctrl+Q")) {
                            app.quit();
                        }

                        ImGui::EndMenu();
                    }

                    ImGui::EndMainMenuBar();
                }

                custom_update(dockspace_id);
                render_log_window();

                ImGui::Render();
            }

            void render() {
                ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

                SDL_Window * backup_current_window = SDL_GL_GetCurrentWindow();
                SDL_GLContext backup_current_context = SDL_GL_GetCurrentContext();
                ImGui::UpdatePlatformWindows();
                ImGui::RenderPlatformWindowsDefault();
                SDL_GL_MakeCurrent(backup_current_window, backup_current_context);
            }

            template <class Caption>
            void text(const Caption & caption) {
                ImGui::PushID(c_str(caption));
                ImGui::TextUnformatted(c_str(caption));
                ImGui::PopID();
            }

            template <class E, size_t N, class Caption>
            bool select_radio(const Caption & caption, E & value, const std::pair<E, const char *> (&values)[N]) {
                ImGui::TextUnformatted(c_str(caption));
                bool result = false;

                for (auto b = std::begin(values), e = std::end(values), it = b; it != e; ++it) {
                    if (it != b) {
                        ImGui::SameLine();
                    }

                    if (ImGui::RadioButton(it->second, value == it->first)) {
                        value = it->first;
                        result = true;
                    }
                }

                ImGui::Separator();

                return result;
            }

            template <class E, class Caption, class String>
            bool select_combo(const Caption & caption, E & value, const std::map<E, String> & values) {
                ImGui::TextUnformatted(c_str(caption));
                ImGui::PushID(c_str(caption));
                bool result = false;

                auto it = values.find(value);

                if (ImGui::BeginCombo("", it != values.end() ? c_str(it->second) : "---")) {
                    for (auto b = std::begin(values), e = std::end(values), it = b; it != e; ++it) {
                        if (ImGui::Selectable(c_str(it->second), value == it->first)) {
                            value = it->first;
                            result = true;
                        }

                        if (value == it->first) {
                            ImGui::SetItemDefaultFocus();
                        }
                    }

                    ImGui::EndCombo();
                }

                ImGui::PopID();
                ImGui::Separator();

                return result;
            }

            template <class Caption>
            bool slider(const Caption & caption, math::int_size & value, const math::int_size & min, const math::int_size & max) {
                bool result = false;
                float full_width = ImGui::GetWindowContentRegionWidth();

                ImGui::TextUnformatted(c_str(caption));

                ImGui::PushItemWidth(full_width / 2 - 1); {
                    ImGui::PushID(&value.x);
                    result |= ImGui::SliderInt("", &value.x, min.x, max.x);
                    ImGui::PopID();
                    ImGui::SameLine(0, 2.0f);
                    ImGui::PushID(&value.y);
                    result |= ImGui::SliderInt("", &value.y, min.y, max.y);
                    ImGui::PopID();
                }
                ImGui::PopItemWidth();

                ImGui::Separator();

                return result;
            }

            template <class Caption>
            bool slider(const Caption & caption, int & value, int min, int max) {
                ImGui::TextUnformatted(c_str(caption));
                ImGui::PushID(c_str(caption));
                bool result = ImGui::SliderInt("", &value, min, max);
                ImGui::PopID();
                ImGui::Separator();

                return result;
            }

            template <class Caption>
            bool slider(const Caption & caption, float & value, float min, float max) {
                ImGui::TextUnformatted(c_str(caption));
                ImGui::PushID(c_str(caption));
                bool result = ImGui::SliderFloat("", &value, min, max);
                ImGui::PopID();
                ImGui::Separator();

                return result;
            }

            template <class Caption>
            bool input(const Caption & caption, int & value, int min, int max) {
                ImGui::TextUnformatted(c_str(caption));
                ImGui::PushID(c_str(caption));
                bool result = ImGui::InputInt("", &value, min, max);
                ImGui::PopID();
                ImGui::Separator();

                return result;
            }

            template <class Caption>
            bool input(const Caption & caption, float & value, float min, float max) {
                ImGui::TextUnformatted(c_str(caption));
                ImGui::PushID(c_str(caption));
                bool result = ImGui::InputFloat("", &value, min, max);
                ImGui::PopID();
                ImGui::Separator();

                return result;
            }

        private:
            void render_log_window() {
                ImGui::Begin("Log"); {
                    ImGui::PushItemWidth(-1.0f);

                    ImGui::PushFont(log_font);

                    for (auto & line : log_buffer->lines()) {
                        text(line);
                    }

                    ImGui::PopFont();

                    ImGui::PopItemWidth();
                    ImGui::SetScrollHereY(1.0f);
                }

                ImGui::End();
            }
        };
    }
}
