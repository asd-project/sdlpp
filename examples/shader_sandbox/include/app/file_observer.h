//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <boost/predef.h>

#if BOOST_OS_WINDOWS

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#elif BOOST_OS_MACOS

#include <sys/event.h>

#endif

#include <array>
#include <vector>
#include <map>
#include <unordered_map>
#include <chrono>
#include <string>
#include <regex>
#include <algorithm>
#include <functional>
#include <iostream>
#include <variant>
#include <utility>

#include <filesystem>
#include <type_traits>

//---------------------------------------------------------------------------

namespace asd
{
    namespace fs
    {
        using namespace std::filesystem;
    }

    namespace app
    {
        enum class file_change_event
        {
            added,
            removed,
            modified
        };

        using file_observer_target_id = int;

        struct file_change_info
        {
            file_observer_target_id target_id;
            fs::path path;
            file_change_event event;
        };

        class file_observer
        {
        public:
            file_observer();
            ~file_observer();

            file_observer(file_observer && other) = default;
            file_observer & operator = (file_observer &&) = default;

            file_observer_target_id add(const fs::path & path, const std::regex & pattern = std::regex(".*")) {
#if BOOST_OS_LINUX || BOOST_OS_MACOS
                target_info target{this, fs::weakly_canonical(path), pattern};
                auto id = target.watch;
                _targets.emplace(id, std::move(target));

                return id;
#elif BOOST_OS_WINDOWS
                _targets.emplace(
                    std::piecewise_construct,
                    std::make_tuple(_last_id),
                    std::make_tuple(this, fs::weakly_canonical(path), pattern)
                );

                return _last_id++;
#endif
                return -1;
            }

            void remove(file_observer_target_id id) {
                _targets.erase(id);
            }

            void clear() {
                _targets.clear();
            }

            template <class F>
            void update(F callback, std::chrono::milliseconds timeout = std::chrono::milliseconds{0}) {
                update_targets(timeout);

                if (!_changed_files.empty()) {
                    callback(_changed_files);
                    _changed_files.clear();
                }
            }

        private:
            void update_targets(std::chrono::milliseconds timeout);

#if BOOST_OS_WINDOWS

            struct target_info
            {
                target_info(file_observer * watcher, const fs::path & path, const std::regex & pattern);
                target_info(target_info &&) = default;
                ~target_info();

                std::variant<fs::path, std::regex> filter;
                HANDLE handle = nullptr;
                OVERLAPPED watch = {0};
                bool need_refresh = true;
            };

            std::array<uint8_t, 1024 * 256> _buffer{0};
            file_observer_target_id _last_id;

#elif BOOST_OS_LINUX

            struct target_info
            {
                target_info(file_observer * watcher, const fs::path & path, const std::regex & pattern);
                target_info(target_info &&) = default;
                ~target_info();

                file_observer * watcher;
                std::variant<fs::path, std::regex> filter;
                int watch = -1;
            };

            int _handle = -1;
            std::array<uint8_t, 1024 * 256> _buffer{0};

#elif BOOST_OS_MACOS

            struct target_info
            {
                target_info(file_observer * watcher, const fs::path & path, const std::regex & pattern);
                target_info(target_info && t) :
                    watcher(t.watcher),
                    pattern(std::move(t.pattern)),
                    watch(std::exchange(t.watch, -1)),
                    base_path(std::move(t.base_path)),
                    is_file(t.is_file),
                    handles(std::move(t.handles)),
                    filenames(std::move(t.filenames))
                    {}
                ~target_info();

                void scan(std::vector<file_change_info> & changes);
                void clear();

                file_observer * watcher;
                std::regex pattern;
                int watch = -1;
                fs::path base_path;
                bool is_file = false;
                std::map<fs::path, int> handles;
                std::unordered_map<int, fs::path> filenames;
            };

            int _handle = -1;
            std::vector<struct kevent> _changes;
#elif ASD_PLAT_EMSCRIPTEN
            struct target_info {};
#endif

            std::vector<file_change_info> _changed_files;
            std::unordered_map<file_observer_target_id, target_info> _targets;
        };
    }
}
