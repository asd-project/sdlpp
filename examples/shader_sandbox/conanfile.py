import os

from conans import ConanFile, CMake

project_name = "shader_sandbox"


class ShaderSandboxConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    settings = "os", "compiler", "build_type", "arch"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.launch/0.0.1@asd/testing",
        "asd.scene/0.0.1@asd/testing",
        "asd.sdlpp/0.0.1@asd/testing",
        "asd.gfx3d_templates/0.0.1@asd/testing",
        "asd.fs/0.0.1@asd/testing",
        "asd.uio/0.0.1@asd/testing",
        "di/[>=1.2]",
		"asd.imgui/1.79.0@asd/testing",
        "spdlog/[~=1.8]"
    )

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", src="include", dst="include")
        self.copy("*shader_sandbox*", src="bin", dst="bin", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = []
