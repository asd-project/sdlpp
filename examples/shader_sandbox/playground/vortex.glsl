//!vertex: p2 t2
#version 300 es
precision highp float;

in vec2 position;
in vec2 texcoord;

out vec2 vs_texcoord;

void main(void)
{
    vs_texcoord = texcoord;
    gl_Position = vec4(position, 0.0, 1.0);
}



//!fragment
#version 300 es
precision highp float;

layout(std140) uniform Resolution
{
    vec4 resolution;
};

layout(std140) uniform Time
{
    float time;
};

in vec2 vs_texcoord;

out vec4 fs_color;

vec2 rotate(vec2 v, float angle) {
    vec2 cs = vec2(cos(angle), sin(angle));
    return v * mat2(cs.x, -cs.y, cs.y, cs.x);
}

void main(void) {
    vec2 uv = vec2(1.0, resolution.y / resolution.x) * (vs_texcoord * 2.0 - 1.0);
    float d = dot(uv, uv);
    float t = 3.1415 * time;
    float scaling = (1.0 + 0.5 * sin(0.5 * t)) * 8.;
    float pulse = (0.5 + 0.25 * sin(0.5 * t));

    uv = rotate(uv, time * 0.4 - (pulse - 0.25) * sqrt(d) * 4.0) * 1.5;
    float pattern = abs(uv.y * uv.y - uv.x * uv.x) * scaling;
    float id = floor(pattern);
    float v = (0.5 - abs(fract(pattern) - 0.5)) / 4. * pulse;
    vec3 col = vec3(smoothstep(0.04, 0.00, v - 0.001));
    id = cos(id * 0.25) * 4. + 2.0;
    vec3 offset = mix(vec3(0.0, 0.5, 1.0), vec3(0.0, 0.0, 1.0), id * 0.125);
    
    col += offset;
    col += 0.25 * smoothstep(2.0, 0.0, 4.0 * pulse * d);

    // vec3 col = vec3(uv, 0.0);
    fs_color = vec4(col, 1.0);
}
