//!vertex: p2 t2
#version 330 core
precision highp float;

in vec2 position;
in vec2 texcoord;

out vec2 vs_texcoord;

void main(void)
{
    vs_texcoord = texcoord;
    gl_Position = vec4(position, 0.0, 1.0);
}

//!fragment
#version 330 core
precision highp float;

layout(std140) uniform Resolution
{
    vec4 resolution;
};

layout(std140) uniform Time
{
    float time;
};

layout(std140) uniform Navigation
{
    vec2 panning;
    float zoom;
};

in vec2 vs_texcoord;

out vec4 fs_color;
vec2 complexMult(vec2 a, vec2 b) {
	return vec2(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}

float testMandelbrot(vec2 coord) {
    // turn this up to 5000 or so if you have a good gpu
    // for better details but less vibrant color in extreme zoom
    const int iterations = 912;
	vec2 testPoint = vec2(0,0);
	for (int i = 0; i < iterations; i++){
		testPoint = complexMult(testPoint,testPoint) + coord;
        float ndot = dot(testPoint,testPoint);
		if (ndot > 45678.0) {
            float sl = float(i) - log2(log2(ndot))+4.0;
			return sl/float(iterations);
		}
	}
	return 0.0;
}

vec4 mapColor(float mcol) {
    return vec4(0.5 + 0.5*cos(2.7+mcol*30.0 + vec3(0.0,.6,1.0)),1.0);
}

void main() {
        // vec2(-1.1553,0.545105) is good too
    const vec2 zoomP = vec2(-.7457117,.186142);
    const float zoomTime = 100.0;
    float tTime = 9.0 + abs(mod(time+zoomTime,zoomTime*2.0)-zoomTime);
    tTime = (145.5/(.0005*pow(tTime,5.0)));
    vec2 aspect = vec2(1,resolution.y/resolution.x);
    vec2 fragment = gl_FragCoord.xy/resolution.xy;
    vec2 mouse = panning.xy/resolution.xy;
	vec2 uv = aspect * (zoomP + tTime * (fragment - mouse));

	fs_color = mapColor(testMandelbrot(uv));
}