//!vertex: p2 t2
#version 330 core

in vec2 position;
in vec2 texcoord;

out vec2 vs_texcoord;

void main(void) {
    vs_texcoord = texcoord;
    gl_Position = vec4(position, 0.0, 1.0);
}

//!fragment
#version 330 core

layout(std140) uniform Resolution {
    vec4 resolution;
};

layout(std140) uniform Time {
    float time;
};

layout(std140) uniform Navigation
{
    vec2 panning;
    float zoom;
} nav;

in vec2 vs_texcoord;

out vec4 fs_color;

void main(void) {
    vec2 uv = vec2(1.0, resolution.y / resolution.x) * (vs_texcoord * 2.0 - 1.0);
    uv = uv / nav.zoom - nav.panning / resolution.x;

    uv *= mat2(0.707, -0.707, 0.707, 0.707);
    uv *= 15.0;

    float angle = 0.0;//atan(uv.y, uv.x);
    uv += 0.5;
    vec2 gv = fract(uv) - 0.5;
    vec2 id = floor(uv);

    vec3 m = vec3(0.);

    for (float y = -1.0; y <= 1.0; y++) {
        for (float x = -1.0; x <= 1.0; x++) {
            vec2 offs = vec2(x, y);

            float t = length(id - offs) * 0.2 - time;
            float r = mix(.7, 1.6, sin(t + angle) * 0.5 + 0.5);
            float c = smoothstep(r, r * (1.0 - 0.1 / nav.zoom), length(gv + offs));
            
            m = m * (1.0 - c) + c * (1.0 - m);
            // m.y = m.z * (1.0 - c) + c * (1.0 - m.x);
            // m.z = m.x * (1.0 - c) + c * (1.0 - m.y);
        }
    }

    fs_color = vec4(1.0 - m, length(m));
}
