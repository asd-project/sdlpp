//!vertex: p2 t2
#version 330 core
precision highp float;

in vec2 position;
in vec2 texcoord;

out vec2 vs_texcoord;

void main(void)
{
    vs_texcoord = texcoord;
    gl_Position = vec4(position, 0.0, 1.0);
}

//!fragment
#version 330 core
precision highp float;

layout(std140) uniform Resolution
{
    vec4 resolution;
};

layout(std140) uniform Time
{
    float time;
};

layout(std140) uniform Navigation
{
    vec2 panning;
    float zoom;
};

in vec2 vs_texcoord;

out vec4 fs_color;

vec2 rotate(vec2 v, float angle) {
    vec2 cs = vec2(cos(angle), sin(angle));
    return v * mat2(cs.x, -cs.y, cs.y, cs.x);
}

void main(void) {
    vec2 uv = vec2(1.0, -1.0) * (2.0 * gl_FragCoord.xy - resolution.xy) / resolution.x;
    vec2 c = (uv / zoom - panning / resolution.x) * 3.0;
    vec2 z = vec2(0.0);

    float i = 0.0;
    float iterations = 1000.0;// * (0.01 * sin(time) + 1.5);

    for (; i < iterations; ++i) {
        z = vec2(z.x * z.x - z.y * z.y, 2.0 * z.x * z.y) + c;

        if (dot(z, z) > 4.0) {
            break;
        }
    }

    float v = sqrt(i / iterations);
    vec3 col = vec3(v);
    fs_color = vec4(col, 1.0);
}
