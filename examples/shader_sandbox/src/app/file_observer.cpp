//---------------------------------------------------------------------------

#include <app/file_observer.h>

#if BOOST_OS_WINDOWS

#include <Pathcch.h>
#include <shlwapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>

#elif BOOST_OS_LINUX

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/inotify.h>
#include <sys/stat.h>
#include <sys/types.h>

#elif BOOST_OS_MACOS

#include <sys/event.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

#endif

#include <system_error>

#include <container/set.h>
#include <meta/map.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
#if BOOST_OS_WINDOWS

        static constexpr DWORD listen_filters = FILE_NOTIFY_CHANGE_SECURITY | FILE_NOTIFY_CHANGE_CREATION | FILE_NOTIFY_CHANGE_LAST_ACCESS | FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_SIZE | FILE_NOTIFY_CHANGE_ATTRIBUTES | FILE_NOTIFY_CHANGE_DIR_NAME | FILE_NOTIFY_CHANGE_FILE_NAME;

        static constexpr auto event_type_mapping = meta::make_map<DWORD, file_change_event>({
            {FILE_ACTION_ADDED, file_change_event::added},
            {FILE_ACTION_REMOVED, file_change_event::removed},
            {FILE_ACTION_MODIFIED, file_change_event::modified},
            {FILE_ACTION_RENAMED_OLD_NAME, file_change_event::removed},
            {FILE_ACTION_RENAMED_NEW_NAME, file_change_event::added}
        });

        file_observer::target_info::target_info(file_observer * watcher, const fs::path & path, const std::regex & pattern) {
            fs::path watch_path;

            if (fs::is_regular_file(path)) {
                filter = path.filename();
                watch_path = path.parent_path();
            } else {
                filter = pattern;
                watch_path = path;
            }

            handle = CreateFileW(
                watch_path.c_str(),
                FILE_LIST_DIRECTORY,                                    // access (read/write) mode
                FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, // share mode
                nullptr,                                                // security descriptor
                OPEN_EXISTING,                                          // how to create
                FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED,      // file attributes
                HANDLE(0)                                               // file with attributes to copy
            );

            if (handle == INVALID_HANDLE_VALUE) {
                BOOST_THROW_EXCEPTION(std::system_error(GetLastError(), std::system_category()));
            }

            watch.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

            if (!watch.hEvent) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Error creating monitor event"));
            }
        }

        file_observer::target_info::~target_info() {
            if (!handle) {
                return;
            }

            if (need_refresh) {
                DWORD bytes_returned = 0;

                CancelIo(handle);
                GetOverlappedResult(handle, &watch, &bytes_returned, TRUE);
            }

            CloseHandle(handle);
        }

        file_observer::file_observer() {}
        file_observer::~file_observer() {}

        void file_observer::update_targets(std::chrono::milliseconds timeout) {
            constexpr auto pass_filter = [](const file_observer::target_info & target, const fs::path & file_path) {
                if (target.filter.index() == 0) {
                    return file_path.filename() == get<fs::path>(target.filter);
                }

                return std::regex_match(file_path.filename().string(), get<std::regex>(target.filter));
            };

            for (auto & [id, target] : _targets) {
                if (target.need_refresh) {
                    DWORD bytes_returned = 0;

                    ReadDirectoryChangesW(
                        target.handle,
                        _buffer.data(), static_cast<DWORD>(_buffer.size()),
                        TRUE,
                        listen_filters,
                        &bytes_returned,
                        &target.watch,
                        NULL
                    );
                }

                switch (WaitForMultipleObjects(1, &target.watch.hEvent, FALSE, static_cast<DWORD>(timeout.count()))) {
                    case WAIT_OBJECT_0: {
                        DWORD bytes_returned = 0;

                        if (!GetOverlappedResult(target.handle, &target.watch, &bytes_returned, TRUE)) {
                            BOOST_THROW_EXCEPTION(std::system_error(GetLastError(), std::system_category()));
                        }

                        target.need_refresh = true;

                        if (bytes_returned == 0) {
                            break;
                        }

                        FILE_NOTIFY_INFORMATION * file_information = reinterpret_cast<FILE_NOTIFY_INFORMATION *>(&_buffer[0]);

                        do {
                            fs::path changed_file = std::wstring{file_information->FileName, file_information->FileNameLength / sizeof(file_information->FileName[0])};

                            if (pass_filter(target, changed_file)) {
                                _changed_files.push_back({id, changed_file, event_type_mapping.at(file_information->Action)});
                            }

                            if (file_information->NextEntryOffset == 0) {
                                break;
                            }

                            file_information = reinterpret_cast<FILE_NOTIFY_INFORMATION *>(reinterpret_cast<BYTE *>(file_information) + file_information->NextEntryOffset);
                        } while (true);
                        break;
                    }

                    case WAIT_TIMEOUT:
                        break;

                    case WAIT_FAILED:
                    default:
                        target.need_refresh = true;
                        break;
                }
            }
        }

#elif BOOST_OS_LINUX

        static constexpr std::uint32_t listen_filters = IN_MODIFY | IN_CREATE | IN_DELETE | IN_MOVED_TO | IN_MOVED_FROM;

        file_observer::target_info::target_info(file_observer * watcher, const fs::path & path, const std::regex & pattern) : watcher(watcher) {
            fs::path watch_path;

            if (fs::is_regular_file(path)) {
                filter = path.filename();
                watch_path = path.parent_path();
            } else {
                filter = pattern;
                watch_path = path;
            }

            watch = inotify_add_watch(watcher->_handle, watch_path.c_str(), listen_filters);

            if (watch < 0) {
                BOOST_THROW_EXCEPTION(std::system_error(errno, std::system_category()));
            }
        }

        file_observer::target_info::~target_info() {
            if (watch < 0) {
                return;
            }

            inotify_rm_watch(watcher->_handle, watch);
        }

        file_observer::file_observer() :
            _handle(inotify_init1(IN_NONBLOCK))
        {
            if (_handle < 0) {
                BOOST_THROW_EXCEPTION(std::system_error(errno, std::system_category()));
            }
        }

        file_observer::~file_observer() {
            if (_handle < 0) {
                return;
            }

            _targets.clear();
            ::close(_handle);
        }

        void file_observer::update_targets(std::chrono::milliseconds timeout) {
            auto pass_filter = [](const file_observer::target_info & target, const fs::path & file_path) {
                if (target.filter.index() == 0) {
                    return file_path.filename() == get<fs::path>(target.filter);
                }

                return std::regex_match(file_path.filename().string(), get<std::regex>(target.filter));
            };

            while (true) {
                const auto length = ::read(_handle, static_cast<void *>(_buffer.data()), _buffer.size());

                if (length == -1 && errno != EAGAIN) {
                    std::cerr << "[file_observer] Failed to read inotify events\n";
                    return;
                }

                if (length <= 0) {
                    return;
                }

                for (int i = 0; i < length;) {
                    struct inotify_event * event = reinterpret_cast<struct inotify_event *>(&_buffer[i]);

                    if (event->len > 0) {
                        auto & target = _targets.at(event->wd);
                        const fs::path changed_file {event->name};

                        if (pass_filter(target, changed_file)) {
                            if (event->mask & IN_CREATE || event->mask & IN_MOVED_TO) {
                                _changed_files.push_back({event->wd, changed_file, file_change_event::added});
                            } else if (event->mask & IN_DELETE || event->mask & IN_MOVED_FROM) {
                                _changed_files.push_back({event->wd, changed_file, file_change_event::removed});
                            } else if (event->mask & IN_MODIFY) {
                                _changed_files.push_back({event->wd, changed_file, file_change_event::modified});
                            }
                        }
                    }

                    i += sizeof(struct inotify_event) + event->len;
                }
            }
        }

#elif BOOST_OS_MACOS
        file_observer::target_info::target_info(file_observer * watcher, const fs::path & path, const std::regex & pattern) : watcher(watcher) {
            is_file = fs::is_regular_file(path);
            base_path = path;

            if (!is_file) {
                this->pattern = pattern;
            }

            if (!fs::exists(base_path)) {
                return;
            }

            watch = ::open(base_path.c_str(), O_EVTONLY);

            if (watch < 0) {
                std::cerr << "[file_observer] Failed to open " << base_path.string() << "\n";
                return;
            }

            if (is_file) {
                return;
            }

            for (auto & entry : fs::directory_iterator(base_path)) {
                if (!entry.is_regular_file()) {
                    continue;
                }

                auto & path = entry.path();
                auto filename = path.filename();

                if (!std::regex_match(filename.string(), pattern)) {
                    continue;
                }

                int handle = ::open(path.c_str(), O_EVTONLY);

                if (handle < 0) {
                    std::cerr << "[file_observer] Failed to open file " << path.string() << "\n";
                    continue;
                }

                handles.emplace(filename, handle);
                filenames.emplace(handle, filename);
            }
        }

        void file_observer::target_info::scan(std::vector<file_change_info> & changes) {
            if (is_file) {
                return;
            }

            set<int> found_handles;

            for (auto & entry : fs::directory_iterator(base_path)) {
                if (!entry.is_regular_file()) {
                    continue;
                }

                auto & path = entry.path();
                auto filename = path.filename();
                auto it = handles.find(filename);

                if (it != handles.end()) {
                    found_handles.emplace(it->second);
                    continue;
                }

                if (!std::regex_match(filename.string(), pattern)) {
                    continue;
                }

                int handle = ::open(path.c_str(), O_EVTONLY);

                if (handle < 0) {
                    std::cerr << "[file_observer] Failed to open file " << path.string() << "\n";
                    continue;
                }

                found_handles.emplace(handle);
                handles.emplace(filename, handle);
                filenames.emplace(handle, filename);

                changes.push_back({watch, path, file_change_event::added});
            }

            for (auto it = filenames.begin(); it != filenames.end();) {
                if (found_handles.find(it->first) != found_handles.end()) {
                    ++it;
                    continue;
                }

                changes.push_back({watch, base_path / it->second, file_change_event::removed});
                ::close(it->first);

                handles.erase(it->second);
                it = filenames.erase(it);
            }
        }

        void file_observer::target_info::clear() {
            if (watch < 0) {
                return;
            }

            ::close(watch);
            watch = -1;

            if (is_file) {
                return;
            }

            for (auto [handle, _] : filenames) {
                ::close(handle);
            }

            handles.clear();
            filenames.clear();
        }

        file_observer::target_info::~target_info() {
            clear();
        }

        file_observer::file_observer() :
            _handle(kqueue())
        {
            if (_handle < 0) {
                BOOST_THROW_EXCEPTION(std::system_error(errno, std::system_category()));
            }
        }

        file_observer::~file_observer() {
            if (_handle < 0) {
                return;
            }

            _targets.clear();
            ::close(_handle);
        }

        template <class T>
        static void print_error(const struct kevent & event, const T & target) {
            if (target.is_file) {
                if (event.ident == target.watch) {
                    std::cerr << "[file_observer] Event with EV_ERROR for " << target.base_path.string() << "\n";
                    return;
                }

                std::cerr << "[file_observer] Event with EV_ERROR\n";
                return;
            }

            if (event.ident == target.watch) {
                std::cerr << "[file_observer] Event with EV_ERROR for " << target.base_path.string() << "\n";
                return;
            }

            auto it = target.filenames.find(event.ident);

            if (it != target.filenames.end()) {
                std::cerr << "[file_observer] Event with EV_ERROR for " << (target.base_path / it->second).string() << "\n";
                return;
            }

            std::cerr << "[file_observer] Event with EV_ERROR\n";
        }

        void file_observer::update_targets(std::chrono::milliseconds timeout) {
            static constexpr auto flags = EV_ADD | EV_ENABLE | EV_CLEAR;
            static constexpr auto fflags = NOTE_DELETE | NOTE_RENAME | NOTE_WRITE | NOTE_EXTEND | NOTE_ATTRIB | NOTE_LINK | NOTE_REVOKE;

            struct timespec ts;
            ts.tv_sec = timeout.count() / 1000;
            ts.tv_nsec = (timeout.count() % 1000) * 1'000'000;

            _changes.clear();

            for (auto & [id, target] : _targets) {
                if (target.watch < 0) {
                    if (!fs::exists(target.base_path)) {
                        continue;
                    }

                    target.watch = ::open(target.base_path.c_str(), O_EVTONLY);

                    if (target.watch < 0) {
                        std::cerr << "[file_observer] Failed to open " << target.base_path.string() << "\n";
                        continue;
                    }

                    if (target.is_file) {
                        _changed_files.push_back({id, target.base_path, file_change_event::added});
                    } else {
                        target.scan(_changed_files);
                    }
                }

                auto & change = _changes.emplace_back();
                EV_SET(&change, target.watch, EVFILT_VNODE, flags, fflags, 0, &target);

                for (auto & [handle, _] : target.filenames) {
                    auto & change = _changes.emplace_back();
                    EV_SET(&change, handle, EVFILT_VNODE, flags, fflags, 0, &target);
                }
            }

            int changes_count = static_cast<int>(_changes.size());
            int events_count = kevent(_handle, _changes.data(), changes_count, _changes.data(), changes_count, &ts);

            if (events_count == -1) {
                if (errno != EINTR) {
                    std::cerr << "[file_observer] Invalid events count returned by kevent\n";
                }

                return;
            }

            std::vector<target_info *> invalidated_targets;

            for (int i = 0; i < events_count; ++i) {
                auto & event = _changes[i];

                if ((event.flags & EV_ERROR) != 0) {
                    if (event.udata) {
                        auto & target = *static_cast<target_info *>(event.udata);
                        print_error(event, target);
                    } else {
                        std::cerr << "[file_observer] Event with EV_ERROR\n";
                    }

                    continue;
                }

                auto & target = *static_cast<target_info *>(event.udata);

                if ((event.fflags & NOTE_DELETE) != 0) {
                    if (event.ident == target.watch) {
                        if (target.is_file) {
                            _changed_files.push_back({target.watch, target.base_path, file_change_event::removed});
                        }

                        target.clear();
                        continue;
                    }

                    auto it = target.filenames.find(event.ident);

                    if (it != target.filenames.end()) {
                        _changed_files.push_back({target.watch, it->second, file_change_event::removed});

                        target.handles.erase(it->second);
                        target.filenames.erase(it);
                        ::close(event.ident);
                    }
                } else if ((event.fflags & NOTE_RENAME) != 0 || (event.fflags & NOTE_REVOKE) != 0) {
                    if (event.ident == target.watch) {
                        if (target.is_file) {
                            _changed_files.push_back({target.watch, target.base_path, file_change_event::removed});
                        }

                        target.clear();
                        continue;
                    }

                    invalidated_targets.push_back(&target);
                } else if ((event.fflags & NOTE_WRITE) != 0 || (event.fflags & NOTE_ATTRIB) != 0) {
                    if (event.ident == target.watch) {
                        if (target.is_file) {
                            _changed_files.push_back({target.watch, target.base_path, file_change_event::modified});
                        } else {
                            if ((event.fflags & NOTE_WRITE) != 0) {
                                invalidated_targets.push_back(&target);
                            }
                        }

                        continue;
                    }

                    _changed_files.push_back({target.watch, target.base_path / target.filenames[event.ident], file_change_event::modified});
                }
            }

            for (auto target : invalidated_targets) {
                target->scan(_changed_files);
            }
        }
#elif ASD_PLAT_EMSCRIPTEN
#warning "file_observer is not supported on emscripten platform"

        file_observer::file_observer() {}
        file_observer::~file_observer() {}
        void file_observer::update_targets(std::chrono::milliseconds timeout) {}
#endif

    }
}

//---------------------------------------------------------------------------
