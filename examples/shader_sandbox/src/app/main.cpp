//---------------------------------------------------------------------------

#include <launch/main.h>
#include <flow/run_main.h>
#include <flow/timer.h>

#include <scene/camera.h>

#include <app/file_observer.h>
#include <app/gui.h>

#include <fs/fs.h>
#include <gfx3d_templates/mesh_quad.h>
#include <uio/mouse_navigator.h>

#include <spdlog/spdlog.h>
#include <fmt/format.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        using namespace std::chrono_literals;
        using namespace color::literals;

        constexpr color::linear_rgb clear_color = 0x000_rgb;

        struct graphics
        {
            sdl::graphics & context;
            gfx::uniform_registry<opengl::graphics> & uniforms;
            gfx::shader_registry<opengl::graphics> & shaders;
            gfx::render_pass_registry<opengl::graphics> & passes;
            gfx::pipeline_registry<opengl::graphics> & pipelines;
        };

        static constexpr auto Resolution = gfx::create_uniform_layout("Resolution",
            gfx::uniforms::element<space::vector>{"resolution"}
        );

        static constexpr auto Time = gfx::create_uniform_layout("Time",
            gfx::uniforms::element<space::real>{"time"}
        );

        static constexpr auto Navigation = gfx::create_uniform_layout("Navigation",
            gfx::uniforms::element<space::point>{"panning"},
            gfx::uniforms::element<space::real>{"zoom"}
        );

        struct view
        {
            using quads = gfx::mesh_quad_factory<gfx::vertex_layouts::p2t2>;

            view(app::gui & gui, uio::mouse_input & mouse, sdl::window & window, gfx::uniform_registry<opengl::graphics> & uniforms, opengl::shader_registry & shaders, gfx::render_target_registry<opengl::graphics> & render_targets, gfx::render_pass_registry<opengl::graphics> & passes, gfx::mesh_registry<opengl::graphics> & meshes) :
                gui(gui),
                mouse(mouse),
                window(window),
                camera_view(uniforms),
                mouse_navigator(mouse),
                resolution_uniform(uniforms.create(Resolution)),
                time_uniform(uniforms.create(Time)),
                navigation_uniform(uniforms.create(Navigation)),
                shaders(shaders),
                render_target(render_targets.create({math::uint_size(viewport.size())})),
                viewport_texture(render_target->get_buffer(0)),
                main_pass(passes.create({{}, gfx::blending::none, clear_color})),
                viewport_pass(passes.create({*render_target, {}, gfx::blending::interpolative, 0xfff_rgb})),
                quad_mesh(quads::create(meshes))
            {
                camera.set_projection(scene::projection::screen);
                camera.set_far_distance(1.0f);
                camera.set_position({0.0f, 0.0f, -1.0f});
                camera.set_direction({0.0f, 0.0f, 1.0f});

                mouse_navigator.set_zoom_speed(0.05f);

                // mouse.on_press.subscribe([](const auto & e) {
                //     auto pos = e.mouse.position();
                //     spdlog::info("Pressed mouse at {}, {}", pos.x, pos.y);
                // });

                // mouse.on_release.subscribe([](const auto & e) {
                //     auto pos = e.mouse.position();
                //     spdlog::info("Released mouse {}, {}", pos.x, pos.y);
                // });
            }

            void update(float elapsed) {
                time_uniform.assign(elapsed);
                navigation_uniform.assign(mouse_navigator.panning() * 4.0f, mouse_navigator.zoom());

                camera_view.update(camera);
            }

            void render_viewport(const math::int_rect & v) {
                if (shader == nullptr) {
                    return;
                }

                if (viewport != v) {
                    viewport = v;

                    render_target->resize(math::uint_size(viewport.size()));
                    camera.set_viewport(viewport.size());
                    resolution_uniform.assign(space::vector{viewport.width(), viewport.height()});
                }

                gui.graphics.set_viewport(viewport.size());
                viewport_pass->start();

                shader->activate();

                camera_view.activate();
                resolution_uniform.activate();
                time_uniform.activate();
                navigation_uniform.activate();

                quad_mesh->render();

                viewport_pass->end();
            }

            void render() {
                gui.graphics.set_viewport(window.physical_size());
                main_pass->start();
                gui.render();
                main_pass->end();
            }

            void set_shader(gfx::handle<opengl::shader_program> && shader) {
                this->shader = std::move(shader);
            }

            app::gui & gui;
            uio::mouse_input & mouse;
            sdl::window & window;
            math::int_rect viewport {0, 0, 640, 640};
            scene::camera camera;
            scene::camera_view<opengl::graphics> camera_view;
            uio::mouse_navigator mouse_navigator;
            gfx::handle<opengl::shader_program> shader;
            gfx::uniform<opengl::graphics, space::vector> resolution_uniform;
            gfx::uniform<opengl::graphics, space::real> time_uniform;
            gfx::uniform<opengl::graphics, space::point, space::real> navigation_uniform;
            opengl::shader_registry & shaders;
            gfx::handle<gfx::render_target<opengl::graphics>> render_target;
            gfx::texture<opengl::graphics> & viewport_texture;
            gfx::handle<gfx::render_pass<opengl::graphics>> main_pass;
            gfx::handle<gfx::render_pass<opengl::graphics>> viewport_pass;
            gfx::handle<gfx::mesh<opengl::graphics>> quad_mesh;
        };

        struct container
        {
            container(app::gui & gui, app::view & view, sdl::graphics & graphics, opengl::shader_registry & shaders, const fs::path & target_path) :
                gui(gui), view(view), graphics(graphics), shaders(shaders), target_path(target_path)
            {
                fs_observer.add(target_path, pattern);

                event_timer = flow::timer::start(io_context, 16ms, [&]() {
                    if (!this->gui.app.process_events()) {
                        io_context.stop();
                        return;
                    }

                    fs_observer.update([this](auto & changes) {
                        for (auto & change : changes) {
                            switch (change.event) {
                                case file_change_event::added:
                                    spdlog::info("File added: {}", change.path.string());
                                    scan_directory();
                                    break;
                                case file_change_event::modified:
                                    spdlog::info("File modified: {}", change.path.string());

                                    if (current_filename_index >= 0 && change.path == this->target_path / filenames[current_filename_index]) {
                                        update_shader();
                                    }

                                    break;
                                case file_change_event::removed:
                                    spdlog::info("File removed: {}", change.path.string());
                                    scan_directory();
                                    break;
                            }
                        }
                    });
                });

                update_timer = flow::stopwatch_timer<std::chrono::milliseconds>::start(io_context, 16ms, [&](const auto & elapsed) {
                    auto current_time = elapsed.count() / 1000.0f;

                    if (current_time >= last_time + 1.0f) {
                        fps = frames / (current_time - last_time);
                        last_time = current_time;
                        frames = 0;
                    }

                    ++frames;

                    this->view.update(current_time);

                    this->gui.update([this](ImGuiID dockspace_id) {
                        auto root_node = ImGui::DockBuilderGetNode(dockspace_id);

                        if (root_node->IsEmpty()) {
                            ImGuiID main_dock = dockspace_id;
                            ImGuiID bottom_dock = ImGui::DockBuilderSplitNode(main_dock, ImGuiDir_Down, 0.25f, NULL, &main_dock);
                            ImGuiID right_dock = ImGui::DockBuilderSplitNode(main_dock, ImGuiDir_Right, 0.25f, NULL, &main_dock);

                            ImGui::DockBuilderDockWindow("Viewport", main_dock);
                            ImGui::DockBuilderDockWindow("Toolbox", right_dock);
                            ImGui::DockBuilderDockWindow("Log", bottom_dock);
                            ImGui::DockBuilderFinish(dockspace_id);
                        }

                        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));

                        if (ImGui::Begin("Viewport")) {
                            ImVec2 min = ImGui::GetWindowContentRegionMin();
                            ImVec2 max = ImGui::GetWindowContentRegionMax();
                            math::float_rect viewport = {min.x, min.y, max.x, max.y};

                            if (ImGui::IsWindowHovered() && this->view.mouse.is_pressed(uio::mouse_button::left)) {
                                this->view.mouse_navigator.enable();
                            } else {
                                this->view.mouse_navigator.disable();
                            }

                            this->view.render_viewport(math::int_rect(viewport * this->view.window.render_scale()));

                            ImGui::Image((ImTextureID)(intptr_t)this->view.viewport_texture.id(), viewport.size(), ImVec2(0, 1), ImVec2(1, 0));
                        }
                        ImGui::End();
                        ImGui::PopStyleVar();

                        if (ImGui::Begin("Toolbox")) {
                            ImGui::PushItemWidth(-1.0f);

                            this->gui.text(fmt::format("Fps: {:.2f}", fps));

                            if (!filenames.empty()) {
                                auto previous_index = current_filename_index;
                                this->gui.select_combo("File", current_filename_index, filenames);

                                if (previous_index != current_filename_index) {
                                    spdlog::info("Change observable file: {}", filenames[current_filename_index]);
                                    update_shader();
                                }
                            }

                            ImGui::PopItemWidth();
                        }
                        ImGui::End();
                    });

                    this->view.render();
                    this->graphics.present();
                });

                scan_directory();

                if (!filenames.empty()) {
                    current_filename_index = 0;
                }
            }

            int run() {
                update_shader();
                flow::run_main(io_context);

                return 0;
            }

            void update_shader() {
                using namespace std::string_view_literals;

                if (current_filename_index < 0) {
                    return;
                }

                auto current_path = target_path / filenames[current_filename_index];

                if (!fs::exists(current_path)) {
                    spdlog::error("File doesn't exist: {}", current_path.string());
                    return;
                }

                auto shader_source = fs::read_string(current_path);

                constexpr auto version_directive = "#version"sv;
                constexpr auto fragment_token = "//!fragment"sv;

                auto vertex_version_pos = shader_source.find(version_directive);

                if (vertex_version_pos == std::string::npos) {
                    spdlog::error("Invalid shader file: {}", current_path.string());
                    return;
                }

                auto fragment_pos = shader_source.find(fragment_token);

                if (fragment_pos == std::string::npos) {
                    spdlog::error("Invalid shader file: {}", current_path.string());
                    return;
                }

                auto fragment_version_pos = shader_source.find(version_directive, fragment_pos);

                if (fragment_version_pos == std::string::npos) {
                    spdlog::error("Invalid shader file: {}", current_path.string());
                    return;
                }

                auto vertex_source = shader_source.substr(vertex_version_pos, fragment_pos - vertex_version_pos);
                auto fragment_source = shader_source.substr(fragment_version_pos);

                try {
                    this->view.set_shader(shaders.create({
                        "shader_sandbox",
                        {
                            {
                                { vertex_source.c_str(),   GL_VERTEX_SHADER },
                                { fragment_source.c_str(), GL_FRAGMENT_SHADER }
                            },

                            gfx::vertex_layouts::p2t2::instance,
                            {{"texture0"}}
                        }
                    }));
                } catch(const std::exception & e) {
                    spdlog::error(e.what());
                }
            }

            void scan_directory() {
                int idx = 0;
                int new_current_index = -1;
                fs::path current_filename = current_filename_index >= 0 ? filenames[current_filename_index] : fs::path{};

                filenames.clear();

                for (auto & entry : fs::directory_iterator(target_path)) {
                    if (!entry.is_regular_file()) {
                        continue;
                    }

                    auto & path = entry.path();
                    auto filename = path.filename();

                    if (!std::regex_match(filename.string(), pattern)) {
                        continue;
                    }

                    if (filename == current_filename) {
                        new_current_index = idx;
                    }

                    filenames.emplace(idx, filename.string());
                    ++idx;
                }

                current_filename_index = new_current_index;
            }

            app::gui & gui;
            app::view & view;
            sdl::graphics & graphics;
            opengl::shader_registry & shaders;
            fs::path target_path;
            std::map<int, std::string> filenames;
            int current_filename_index = -1;
            file_observer fs_observer;
            boost::asio::io_context io_context;
            flow::timer event_timer;
            flow::stopwatch_timer<std::chrono::milliseconds> update_timer;

            float last_time = 0.0f;
            int frames = 0;
            float fps = 0.0f;

            const std::regex pattern{".*\\.glsl"};
        };

        auto window_module() {
            namespace di = boost::di;

#if ASD_OPENGL_ES
            opengl::configuration opengl_config{opengl::profile::es, 3, 0, 0}; // opengl es
#else
            opengl::configuration opengl_config{opengl::profile::core, 3, 3, 0}; // desktop opengl
#endif

            return di::make_injector(
                di::bind<opengl::configuration>().to(std::move(opengl_config)),
                di::bind<sdl::window_title>().to("shader_sandbox"),
#if ASD_PLAT_EMSCRIPTEN
                di::bind<fs::path>().to("/playground"),
#else
                di::bind<fs::path>().to(fs::weakly_canonical(application::root_path() / "../../playground")),
#endif
                di::bind<sdl::window_size>().to(sdl::window_size{640, 640})
            );
        }

        auto root_module = boost::di::make_injector(
            sdl::module(),
            window_module()
        );

        container & c = root_module.create<container &>();

        static entrance open([]() {
            return c.run();
        });
    }
}

//---------------------------------------------------------------------------
