import os

from conans import ConanFile, CMake, tools

project_name = "sdl_test"

class SdlTestConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    settings = "os", "compiler", "build_type", "arch"
    requires = (
        "asd.launch/0.0.1@asd/testing",
        "asd.scene/0.0.1@asd/testing",
        "asd.gfx3d_templates/0.0.1@asd/testing",
        "asd.fs/0.0.1@asd/testing",
        "asd.sdlpp/0.0.1@asd/testing",
        "di/[>=1.2]",
    )

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dylib*", dst="bin", src="lib")
        self.copy('*.so*', dst='bin', src='lib')

    def test(self):
        if not tools.cross_building(self.settings):
            os.chdir("bin")
            self.run(".%ssdl_test" % os.sep)
