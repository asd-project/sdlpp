/**
 *  !vertex: p3 c4 m
 */
#version 330 core

layout(std140) uniform View
{
    mat4 view;
};

layout(std140) uniform Projection
{
    mat4 projection;
};

in vec3 position;
in vec4 color;
in mat4 transform;

out Vertex
{
    vec4 color;
} vs;

void main(void)
{
    vs.color = color;
    gl_Position = projection * view * transform * vec4(position, 1.0);
}
