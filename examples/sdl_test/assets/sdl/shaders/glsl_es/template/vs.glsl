#version 300 es

precision highp float;

/**
 *  !vertex: p3 t2
 */

layout(std140) uniform View
{
    mat4 view;
};

layout(std140) uniform Projection
{
    mat4 projection;
};

in vec3 position;
in vec2 texcoord;
in vec4 color;
in mat4 transform;

out vec2 vs_texcoord;
out vec4 vs_color;

void main(void)
{
    vs_texcoord = texcoord;
    vs_color = color;
    gl_Position = projection * view * transform * vec4(position, 1.0);
}
