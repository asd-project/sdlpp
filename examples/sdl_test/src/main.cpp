//---------------------------------------------------------------------------

#include <launch/main.h>

#include <color/color.h>

#include <gfx/shader.h>
#include <gfx/mesh.h>
#include <gfx/uniform.h>
#include <gfx/render_target.h>
#include <gfx/render_pass.h>
#include <gfx/pipeline.h>

#include <opengl/opengl.h>
#include <opengl/vertex_layout.h>

#include <sdlpp/module.h>

#include <flow/run_main.h>
#include <flow/timer.h>

#include <scene/scene.h>
#include <signal/hub.h>

#include <fs/fs.h>

#include <gfx3d_templates/mesh_quad.h>
#include <gfx3d_templates/mesh_cube.h>

#include <chrono>
#include <random>

#include <math/io.h>

#if ASD_PLAT_EMSCRIPTEN
#include <emscripten/emscripten.h>
#endif

#include <spdlog/spdlog.h>

#include <uio/shortcut.h>

#include <opengl/shaders/embedded.h>

//---------------------------------------------------------------------------

namespace asd
{
    using namespace std::chrono_literals;

    namespace gfx
    {
        namespace vertex_layouts
        {
            using c4m = generator<vertex_attributes::c4, vertex_attributes::m>;
            using m = generator<vertex_attributes::m>;
            using p3c4m = generator<vertex_attributes::p3, vertex_attributes::c4, vertex_attributes::m>;
            using p3t2c4m = generator<vertex_attributes::p3, vertex_attributes::t2, vertex_attributes::c4, vertex_attributes::m>;
        }
    }

    template <class F>
    int loop(sdl::window & window, F job) {
        SDL_Event e;

        while (true) {
            while (SDL_PollEvent(&e) != 0) {
                switch (e.type) {
                    case SDL_WINDOWEVENT: {
                        switch (e.window.event) {
                            case SDL_WINDOWEVENT_SIZE_CHANGED:
                                window.process_event(e.window);
                                break;

                            default: {}
                        }

                        break;
                    }

                    case SDL_QUIT:
                        return 0;

                    default: {}
                }
            }

            job();
        }
    }

    class model
    {
    public:
        template <class Multimesh>
        model(Multimesh & mesh, const color::linear_rgb & color, space::real scaling, const space::vector & translation, space::real color_speed) :
            translation(space::matrix::translation(translation)),
            scaling(space::matrix::scaling(scaling)),
            color(color),
            color_speed(color_speed),
            instance(mesh.instance(color, get_transform()))
        {}

        space::matrix get_transform() {
            return scaling * math::matrix(rotation) * translation;
        }

        void update_transform(const space::matrix & parent) {
            instance.assign<1>(get_transform() * parent);
        }

        void update(space::real ticks) {
            color::rotate_inplace(this->color, color_speed * 0.0005f);
            instance.assign<0>(convert<color::linear_rgb>(this->color));
        }

        space::matrix translation;
        space::quaternion rotation;
        space::matrix scaling;

    private:
        color::hsv color;
        space::real color_speed;
        gfx::multimesh<opengl::graphics, color::linear_rgb, space::matrix> instance;
    };

#if ASD_OPENGL_ES
    using resolution_t = space::vector;
#else
    using resolution_t = space::size;
#endif

    static constexpr auto Resolution = gfx::create_uniform_layout("Resolution",
        gfx::uniforms::element<resolution_t>{"resolution"}
    );

    using namespace gfx;
    using namespace color::literals;

    struct
    {
        sdl::app app;

        sdl::window window{app, "sdl::test", { 768, 768 }};
        sdl::mouse mouse{app, window};
        sdl::keyboard keyboard{app, window};
        sdl::graphics context{window};

        opengl::uniform_registry uniforms{context};
        opengl::shader_registry shaders{uniforms};
        opengl::mesh_registry meshes{context};
        opengl::multimesh_registry multimeshes;
        opengl::render_target_registry targets;
        opengl::render_pass_registry passes{context};
        opengl::pipeline_registry pipelines;

        gfx::uniform_factory<opengl::graphics, space::matrix> view_uniform_factory = uniforms.factory(gfx::uniforms::View);
        gfx::uniform_factory<opengl::graphics, space::matrix> projection_uniform_factory = uniforms.factory(gfx::uniforms::Projection);
        gfx::uniform_factory<opengl::graphics, resolution_t> resolution_uniform_factory = uniforms.factory(Resolution);

        gfx::uniform<opengl::graphics, space::matrix> view_uniform = view_uniform_factory.create(space::matrix::look_at({ 0.0f, 0.0f, -6.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }));
        gfx::uniform<opengl::graphics, space::matrix> projection_uniform = projection_uniform_factory.create(space::matrix::perspective(60.0f, 480.0f / 768.0f, 0.001f, 100.0f));
        gfx::uniform<opengl::graphics, resolution_t> resolution_uniform = resolution_uniform_factory.create(resolution_t(1.0f / window.logical_size()));

        const space::real rotation_speed = 0.5f;

        boost::asio::io_context io_context;
        space::quaternion rotation;

        signal::hub hub;
        uio::shortcut_context shortcuts{keyboard, hub};

        int run() {
            const auto shaders_path = application::root_path() / "shaders" / "template";

            constexpr auto shader_program_name = "3d/color";

            const auto vertex_source = fs::read_string(shaders_path / "vs.glsl");
            const auto fragment_source = fs::read_string(shaders_path / "fs.glsl");

            const auto program = shaders.create({
                shader_program_name,
                {
                    {
                        { vertex_source.c_str(),   GL_VERTEX_SHADER },
                        { fragment_source.c_str(), GL_FRAGMENT_SHADER }
                    },

                    gfx::vertex_layouts::p3c4m::instance,
                    {{ "texture0" }}
                }
            });

            const auto post_process_program = shaders.create({"2d/blur", opengl::shaders["2d/blur"]});

            const auto fill_pipeline = pipelines.create({ *program });
            const auto wireframe_pipeline = pipelines.create({ *program });
            const auto post_process_pipeline = pipelines.create({ *post_process_program });

            using quads = gfx::mesh_quad_factory<gfx::vertex_layouts::p2t2>;
            using cubes = gfx::mesh_cube_factory<gfx::vertex_layouts::p3>;
            using cube_frames = gfx::mesh_cube_frame_factory<gfx::vertex_layouts::p3>;

            const auto quad_mesh = quads::create(meshes);
            const auto cube_mesh = cubes::create<gfx::vertex_layouts::c4m>(multimeshes);
            const auto cube_frame_mesh = cube_frames::create<gfx::vertex_layouts::c4m>(multimeshes);

            array_list<model> models, frame_models;

            auto create_cube = [&](const space::vector & position, const color::linear_rgb & color, space::real color_speed) {
                models.emplace_back(*cube_mesh, color, 0.495f, position, color_speed);
                frame_models.emplace_back(*cube_frame_mesh, color::linear_rgb(0.2f, 0.2f, 0.2f), 0.5f, position, color_speed);
            };

    //        array_list<color::linear_rgb> colors{
    //            { 1.0f, 1.0f, 1.0f },
    //            { 0.0f, 0.0f, 0.0f },
    //            { 0.85f, 0.0f, 0.0f },
    //            { 0.9f, 0.0f, 0.4f },
    //            { 0.1f, 0.2f, 0.9f },
    //            { 1.0f, 0.8f, 0.0f },
    //            { 0.5f, 1.0f, 0.05f },
    //            { 0.0f, 0.8f, 0.9f }
    //        };
    //
    //        const int count = 1000;
    //        const space::real radius = 10.0f;
    //
    //        std::random_device rd;
    //        std::mt19937 gen(rd());
    //        auto coord_rand = std::bind(std::uniform_real_distribution<space::real>(-radius, radius), gen);
    //        auto distance_rand = std::bind(std::uniform_real_distribution<space::real>(0, 1), gen);
    //        auto color_rand = std::bind(std::uniform_int_distribution<size_t>(0, colors.size() - 1), gen);
    //        auto color_speed_rand = std::bind(std::uniform_real_distribution<space::real>(1.0f, 3.0f), gen);
    //
    //        for (int i = 0; i < count; ++i) {
    //            vector v = vector{ coord_rand(), coord_rand(), coord_rand() }.normalized() * math::sqrt(math::sqrt(distance_rand())) * radius;
    //            v = vector { math::round(v.x), math::round(v.y), math::round(v.z) };
    //            create_cube(v, colors[color_rand()], color_speed_rand());
    //        }

            create_cube({  1.0f, -1.0f, -1.0f }, { 1.0f,  1.0f,  1.0f  }, 0.0f);
            create_cube({ -1.0f,  1.0f,  1.0f }, { 0.0f,  0.0f,  0.0f  }, 0.0f);
            create_cube({  1.0f, -1.0f,  1.0f }, { 0.85f, 0.0f,  0.0f  }, 0.0f);
            create_cube({ -1.0f, -1.0f,  1.0f }, { 0.9f,  0.0f,  0.4f  }, 0.0f);
            create_cube({ -1.0f, -1.0f, -1.0f }, { 0.1f,  0.2f,  0.9f  }, 0.0f);
            create_cube({  1.0f,  1.0f,  1.0f }, { 1.0f,  0.8f,  0.0f  }, 0.0f);
            create_cube({  1.0f,  1.0f, -1.0f }, { 0.5f,  1.0f,  0.05f }, 0.0f);
            create_cube({ -1.0f,  1.0f, -1.0f }, { 0.0f,  0.8f,  0.9f  }, 0.0f);

            std::shared_ptr main_target = targets.create({math::uint_size(window.physical_size())});
            auto & target_texture = main_target->add_buffer({gfx::texture_format::rgba});

            const auto main_pass = passes.create({ *main_target, { gfx::feature::depth_test }, gfx::blending::none, 0xf00_rgb, 1.0f });
            const auto post_process_pass = passes.create({ { gfx::feature::depth_test }, gfx::blending::interpolative, 0xf00_rgb, 1.0f });

            hub.subscribe(window.on_resize, [=, this](const sdl::window_size & logical_size, const math::int_size &) {
                main_target->resize(math::uint_size(logical_size / 2));
                auto aspect = logical_size.inverted_ratio<space::real>();
                // projection_uniform = matrix::perspective(60.0f, aspect, 0.1f, 32.0f);
                projection_uniform = space::matrix::ortho(-4.0f / aspect, 4.0f / aspect, -4.0f, 4.0f, 0.1f, 16.0f);
                resolution_uniform = resolution_t{ 2.0f / logical_size };
            });

            hub.subscribe(keyboard.on_release, [this](const uio::keyboard_event & e) {
                if (e.key == uio::keycode::escape) {
                    this->app.quit();
                }
            });

            using namespace uio::literals;

            shortcuts.subscribe("Ctrl+Q"_shortcut, [&](auto &) {
                this->app.quit();
            });

            //SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_OPENGL);

            flow::timer event_timer{io_context, 16ms};
            flow::stopwatch_timer<std::chrono::milliseconds> render_timer{io_context, 16ms};

            event_timer.start([this] () {
                if (!this->app.process_events()) {
                    this->io_context.stop();
                }
            });

            render_timer.start([&, this, prev {0ms}] (auto ms) mutable {
                auto dt = (ms - std::exchange(prev, ms)).count() / 16.0f;

                this->rotation.rotate_x(dt * rotation_speed * 0.01f);
                this->rotation.rotate_z(dt * rotation_speed * 0.01f);
                this->rotation.rotate_y(dt * rotation_speed * 0.01f);

                auto parent = math::matrix(this->rotation);

                for (auto & model : models) {
                    model.update(dt);
                    model.update_transform(parent);
                }

                for (auto & model : frame_models) {
                    model.update_transform(parent);
                }

                this->view_uniform.activate();
                this->projection_uniform.activate();
                this->resolution_uniform.activate();

                {
                    context.set_viewport(math::int_size{window.logical_size() / 2});
                    main_pass->start();

                    fill_pipeline->activate();
                    cube_mesh->render();

                    wireframe_pipeline->activate();
                    cube_frame_mesh->render();

                    main_pass->end();
                }

                {
                    context.set_viewport(window.physical_size());
                    post_process_pass->start();

                    post_process_pipeline->activate();
                    target_texture.bind(0);
                    quad_mesh->render();

                    post_process_pass->end();
                }

                this->context.present();
            });

            flow::run_main(io_context);

            return 0;
        }
    } app_container;

    static entrance open([]() {
        try {
#ifdef ASD_DEBUG
            spdlog::set_level(spdlog::level::trace);
#endif
            return app_container.run();
        } catch (const std::exception & e) {
            spdlog::error(e.what());
            throw e;
        }
    });
}

//---------------------------------------------------------------------------
